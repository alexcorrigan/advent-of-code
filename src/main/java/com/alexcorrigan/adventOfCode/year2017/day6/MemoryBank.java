package com.alexcorrigan.adventOfCode.year2017.day6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MemoryBank {

    private Integer[] memory;
    private List<String> memoryHistory = new ArrayList<>();

    public static MemoryBank memoryBankFrom(String[] rawMemoryBankConfiguration) {
        Integer[] memoryBlocks = Arrays.stream(rawMemoryBankConfiguration).map(Integer::valueOf).collect(Collectors.toList()).toArray(new Integer[]{});
        return new MemoryBank(memoryBlocks);
    }

    public static MemoryBank memoryBankFrom(Integer[] memoryConfiguration) {
        return new MemoryBank(memoryConfiguration);
    }

    private MemoryBank(Integer[] memory) {
        this.memory = memory;
    }


    public RepetitionResult determineNumberOfRedistributionsBeforeRepetition() {
        return redistributeMemoryBlocksUntilRepetition();
    }

    private RepetitionResult redistributeMemoryBlocksUntilRepetition() {
        int distributions = 0;
        memoryHistory.add(toString());

        while (true) {
            performDistributionOfBlocksFromBank(findBankWithMostBlocks());
            distributions ++;
            printMemory();
            if (currentDistributionSeenBefore()) {
                return new RepetitionResult(memory.clone(), distributions);
            } else {
                memoryHistory.add(toString());
            }
        }
    }

    private boolean currentDistributionSeenBefore() {
        return memoryHistory.contains(toString());
    }

    private Integer findBankWithMostBlocks() {
        int mostBlocksInABank = 0;
        Integer bankWithMostBlocks = null;
        for (int bank = 0; bank < memory.length; bank++) {
            int blocksInThisBank = memory[bank];
            if (blocksInThisBank > mostBlocksInABank) {
                mostBlocksInABank = blocksInThisBank;
                bankWithMostBlocks = bank;
            } else if (blocksInThisBank == mostBlocksInABank && (bankWithMostBlocks == null || bankWithMostBlocks > bank)) {
                bankWithMostBlocks = bank;
            }
        }
        return bankWithMostBlocks;
    }

    private void performDistributionOfBlocksFromBank(Integer bankWithMostBlocks) {
        Integer blocksToDistribute = memory[bankWithMostBlocks];
        memory[bankWithMostBlocks] = 0;
        Integer currentBankDistributingTo = determineNextBankFrom(bankWithMostBlocks);
        while (blocksToDistribute > 0) {
            Integer currentBankBlocks = memory[currentBankDistributingTo];
            memory[currentBankDistributingTo] = currentBankBlocks + 1;
            blocksToDistribute --;
            currentBankDistributingTo = determineNextBankFrom(currentBankDistributingTo);
        }
    }

    private int determineNextBankFrom(Integer currentBankDistributingTo) {
        return (currentBankDistributingTo + 1) % memory.length;
    }

    @Override
    public String toString() {
        StringBuilder memoryString = new StringBuilder();
        for (int m = 0; m < memory.length; m++) {
            memoryString.append(memory[m]);
            if (m < memory.length - 1) {
                memoryString.append(", ");
            }
        }
        return memoryString.toString();
    }

    private void printMemory() {
        System.out.println(toString());
    }

}
