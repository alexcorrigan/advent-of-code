package com.alexcorrigan.adventOfCode.year2018.day6;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day6ExamplesTest {


    @Test
    public void part1() {
        List<String> coordinates = new ArrayList<>();
        coordinates.add("1, 1");
        coordinates.add("1, 6");
        coordinates.add("8, 3");
        coordinates.add("3, 4");
        coordinates.add("5, 5");
        coordinates.add("8, 9");

        Grid grid = new Grid(10);
        assertThat(grid.largestNonInfiniteArea(coordinates), is(equalTo(17)));

    }


}
