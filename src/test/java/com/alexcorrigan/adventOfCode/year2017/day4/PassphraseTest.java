package com.alexcorrigan.adventOfCode.year2017.day4;

import org.junit.Test;

import static com.alexcorrigan.adventOfCode.year2017.day4.Passphrase.passhraseFrom;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class PassphraseTest {

    @Test
    public void testNewPassphrase() throws Exception {
        Passphrase passphrase = passhraseFrom("aa bb cc");
        assertThat(passphrase.getPassphraseElements(),  contains("aa", "bb", "cc"));
    }
}
