package com.alexcorrigan.adventOfCode.year2017.day1;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day3ExamplesTest {

    @Test
    public void part1examples() throws Exception {
        CaptchaSolver captchaSolver = new DefaultCaptchaSolver();
        CaptchaSolvingStrategy captchaSolvingStrategy = new NextDigitCaptchaSolvingStrategy();
        assertThat(3, is(equalTo(captchaSolver.solveFor(new Captcha("1122"), captchaSolvingStrategy))));
        assertThat(4, is(equalTo(captchaSolver.solveFor(new Captcha("1111"), captchaSolvingStrategy))));
        assertThat(0, is(equalTo(captchaSolver.solveFor(new Captcha("1234"), captchaSolvingStrategy))));
        assertThat(9, is(equalTo(captchaSolver.solveFor(new Captcha("91212129"), captchaSolvingStrategy))));
    }

    @Test
    public void part2examples() throws Exception {
        CaptchaSolver captchaSolver = new DefaultCaptchaSolver();
        CaptchaSolvingStrategy captchaSolvingStrategy = new HalfWayAroundCaptchaSolvingStrategy();
        assertThat(captchaSolver.solveFor(new Captcha("1212"), captchaSolvingStrategy), is(equalTo(6)));
        assertThat(captchaSolver.solveFor(new Captcha("1221"), captchaSolvingStrategy), is(equalTo(0)));
        assertThat(captchaSolver.solveFor(new Captcha("123425"), captchaSolvingStrategy), is(equalTo(4)));
        assertThat(captchaSolver.solveFor(new Captcha("123123"), captchaSolvingStrategy), is(equalTo(12)));
        assertThat(captchaSolver.solveFor(new Captcha("12131415"), captchaSolvingStrategy), is(equalTo(4)));
    }

}
