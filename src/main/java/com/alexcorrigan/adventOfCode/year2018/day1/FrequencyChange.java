package com.alexcorrigan.adventOfCode.year2018.day1;

public class FrequencyChange {

    private final Integer change;

    public FrequencyChange(String changeString) {
        change = Integer.valueOf(changeString);
    }

    public Integer applyTo(Integer value) {
        return value + change;
    }

    @Override
    public String toString() {
        return String.valueOf(change);
    }
}
