package com.alexcorrigan.adventOfCode.year2018.day1;

import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.DeviceCalibrationStrategy;

public class Device {

    private final DeviceCalibrationStrategy deviceCalibrationStrategy;
    private Frequency calibratedFrequency = new NullFrequency();

    public Device(DeviceCalibrationStrategy deviceCalibrationStrategy) {
        this.deviceCalibrationStrategy = deviceCalibrationStrategy;
    }

    public void calibrate(FrequencyChangeSet frequencyChangeSet, Frequency initialFrequency) {
        calibratedFrequency = deviceCalibrationStrategy.calibrate(initialFrequency, frequencyChangeSet);
    }

    public Frequency calibratedFrequency() {
        return calibratedFrequency;
    }

}
