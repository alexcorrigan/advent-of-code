package com.alexcorrigan.adventOfCode.year2019.day3;

public class IntersectionReport {

    private final Point point;
    private final int manhattanDistanceFromCentralPort;
    private final int blueWireStepsFromCentralPort;
    private final int redWireStepsFromCentralPort;

    public IntersectionReport(Point point, int manhattanDistanceFromCentralPort, int blueWireStepsFromCentralPort, int redWireStepsFromCentralPort) {
        this.point = point;
        this.manhattanDistanceFromCentralPort = manhattanDistanceFromCentralPort;
        this.blueWireStepsFromCentralPort = blueWireStepsFromCentralPort;
        this.redWireStepsFromCentralPort = redWireStepsFromCentralPort;
    }

    public int manhattanDistanceFromCentralPort() {
        return manhattanDistanceFromCentralPort;
    }

    public int combinedStepsFromCentralPort() {
        return blueWireStepsFromCentralPort + redWireStepsFromCentralPort;
    }

}
