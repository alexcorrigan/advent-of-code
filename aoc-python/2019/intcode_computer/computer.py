import logging
from .utils import convert_to_intcode

POSITION = 0
IMMEDIATE = 1

ADDITION = 1
MULTIPLICATION = 2
INPUT = 3
OUTPUT = 4
JUMP_IF_TRUE = 5
JUMP_IF_FALSE = 6

LESS_THAN = 7
EQUALS = 8
STOP = 99

OPCODE_PARAMETER_LENGTHS = {
    ADDITION: 3,
    MULTIPLICATION: 3,
    INPUT: 1,
    OUTPUT: 1,
    JUMP_IF_TRUE: 2,
    JUMP_IF_FALSE: 2,
    LESS_THAN: 3,
    EQUALS: 3,
    STOP: 0
}

OPCODE_OPERATIONS = {
    ADDITION: lambda x, y: x + y,
    MULTIPLICATION: lambda x, y: x * y,
    JUMP_IF_TRUE: lambda x: x != 0,
    JUMP_IF_FALSE: lambda x: x == 0,
    LESS_THAN: lambda x, y: 1 if x < y else 0,
    EQUALS: lambda x, y: 1 if x == y else 0
}

NOUN_POINTER = 1
VERB_POINTER = 2


class Computer:

    def __init__(self, program):
        self._program = list(convert_to_intcode(program))

    def execute_intcode_program(self, noun=None, verb=None):
        current_position = 0

        if noun is not None:
            self._program[NOUN_POINTER] = noun

        if verb is not None:
            self._program[VERB_POINTER] = verb

        while self._program[current_position] != STOP:

            next_position_override = None
            opcode, parameter_modes = self._parse_instruction_at(current_position)
            parameters = self._collect_parameters(opcode, current_position)

            if opcode in [ADDITION, MULTIPLICATION]:
                self._do_calculation_operation(opcode, parameters, parameter_modes)

            elif opcode == INPUT:
                self._process_input(parameters)

            elif opcode == OUTPUT:
                output = self._parameter_mode_based_value(0, parameter_modes, parameters)
                print("output: " + str(output))

            elif opcode in [JUMP_IF_TRUE, JUMP_IF_FALSE]:
                test_parameter = self._parameter_mode_based_value(0, parameter_modes, parameters)
                if OPCODE_OPERATIONS[opcode](test_parameter):
                    next_position_override = self._parameter_mode_based_value(1, parameter_modes, parameters)

            elif opcode in [LESS_THAN, EQUALS]:
                p1 = self._parameter_mode_based_value(0, parameter_modes, parameters)
                p2 = self._parameter_mode_based_value(1, parameter_modes, parameters)
                result_position = parameters[2]
                self._program[result_position] = OPCODE_OPERATIONS[opcode](p1, p2)

            else:
                logging.error("Opcode exception: " + str(opcode))
                raise Exception("SHIT!!! %s" % str(opcode))

            if next_position_override is not None:
                current_position = next_position_override
            else:
                current_position += OPCODE_PARAMETER_LENGTHS[opcode] + 1

        return self._program

    def _parse_instruction_at(self, instruction_pointer):
        instruction_string = str(self._program[instruction_pointer])
        opcode = int(instruction_string[-2:])
        if len(instruction_string) <= 2:
            parameter_modes = [0, 0, 0]
        else:
            parameter_modes = list([str(p) for p in instruction_string[:-2]])
            parameter_modes.reverse()
        return opcode, [int(parameter_mode) for parameter_mode in parameter_modes]

    def _parameter_mode_based_value(self, index, parameter_modes, parameters):
        if index > len(parameter_modes) - 1 or parameter_modes[index] == POSITION:
            return self._program[parameters[index]]

        elif parameter_modes[index] == IMMEDIATE:
            return parameters[index]

        else:
            raise Exception("Unknown parameter mode: " + parameter_modes[index])

    def _collect_parameters(self, opcode, current_position):
        parameters = []
        for parameter_index in range(OPCODE_PARAMETER_LENGTHS[opcode]):
            parameter_index_from_current_position = parameter_index + 1

            value_pointer = self._program[current_position + parameter_index_from_current_position]
            parameters.append(value_pointer)
        return parameters

    def _do_calculation_operation(self, opcode, parameters, parameter_modes):
        a = self._parameter_mode_based_value(0, parameter_modes, parameters)
        b = self._parameter_mode_based_value(1, parameter_modes, parameters)
        result_position = parameters[2]
        self._program[result_position] = OPCODE_OPERATIONS[opcode](a, b)

    def _process_input(self, parameters):
        result = input("give input: ")
        result_position = parameters[0]
        self._program[result_position] = int(result)
