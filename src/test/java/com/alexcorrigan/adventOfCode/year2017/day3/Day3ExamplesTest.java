package com.alexcorrigan.adventOfCode.year2017.day3;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day3ExamplesTest {

    @Test
    public void testPart1Examples() throws Exception {
        SpiralMemory spiralMemory = new SpiralMemory();
        assertThat(spiralMemory.stepsToAccessPortFromMemoryLocation(1), is(equalTo(0)));
        assertThat(spiralMemory.stepsToAccessPortFromMemoryLocation(12), is(equalTo(3)));
        assertThat(spiralMemory.stepsToAccessPortFromMemoryLocation(23), is(equalTo(2)));
        assertThat(spiralMemory.stepsToAccessPortFromMemoryLocation(1024), is(equalTo(31)));
    }

}
