package com.alexcorrigan.adventOfCode.year2017.day4;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_4;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day4Test {

    private PassphraseValidityChecker passphraseValidityChecker;
    private List<Passphrase> passphrases;

    @Before
    public void setUp() throws Exception {
        passphraseValidityChecker = new PassphraseValidityChecker();
        passphrases = loadInputData(DAY_4, "2017").stream().map(Passphrase::passhraseFrom).collect(Collectors.toList());
    }

    @Test
    public void day4Part1() throws Exception {
        ValidityStrategy validityStrategy = new NoDuplicatedWordsValidityStrategy();
        assertValidPassphrases(validityStrategy, 325);
    }

    @Test
    public void day4Part2() throws Exception {
        ValidityStrategy validityStrategy = new NoDuplicatedAnagramsValidityStrategy();
        assertValidPassphrases(validityStrategy, 119);
    }

    private void assertValidPassphrases(ValidityStrategy validityStrategy, int expectedCountOfValidPassphrases) {
        Integer countOfValidPassphrases = (int) passphrases.stream().filter(passphrase -> passphraseValidityChecker.isValid(passphrase, validityStrategy)).count();
        assertThat(countOfValidPassphrases, is(equalTo(expectedCountOfValidPassphrases)));
    }

}
