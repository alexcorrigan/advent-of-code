package com.alexcorrigan.adventOfCode.year2018.day5;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day5ExamplesTest {

    private final char[] types = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    private Map<Character, Integer> removeAndReducePolymerLengths = new HashMap<>();

    @Test
    public void part1Example() {
        assertPolymerReduction("dabAcCaCBAcCcaDA", "dabCBAcaDA");
        assertPolymerReduction("YyLlXxYKkbNnQqBFfxXbyYWwBhHyYTCBbCjIiqwtTWQJczeE", "YTCz");
    }

    @Test
    public void part2Examples() {
        assertRemoveTypeAndReducePolymer("dabAcCaCBAcCcaDA", "a", "dbCBcD");
        assertRemoveTypeAndReducePolymer("dabAcCaCBAcCcaDA", "b", "daCAcaDA");
        assertRemoveTypeAndReducePolymer("dabAcCaCBAcCcaDA", "c", "daDA");
        assertRemoveTypeAndReducePolymer("dabAcCaCBAcCcaDA", "d", "abCBAc");

        for (char typeToRemove : types) {
            Polymer polymer = new Polymer("dabAcCaCBAcCcaDA");
            polymer.removeType("" + typeToRemove);
            polymer.reduce();
            removeAndReducePolymerLengths.put(typeToRemove, polymer.polymerString().length());
        }

        Character bestTypeToRemoveFirst = null;
        int bestLength = Integer.MAX_VALUE;

        for (Character typeRemoved : removeAndReducePolymerLengths.keySet()) {
            if (bestTypeToRemoveFirst == null) {
                bestTypeToRemoveFirst = typeRemoved;
            } else {
                int lengthAfterRemovingType = removeAndReducePolymerLengths.get(typeRemoved);
                if (lengthAfterRemovingType < bestLength) {
                    bestTypeToRemoveFirst = typeRemoved;
                    bestLength = lengthAfterRemovingType;
                }
            }
        }
        assertThat(bestTypeToRemoveFirst, is(equalTo('c')));
        assertThat(bestLength, is(equalTo(4)));
    }

    private void assertPolymerReduction(String polymerString, String expectedReducedPolymer) {
        Polymer polymer = new Polymer(polymerString);
        assertResultingPolymer(polymer, expectedReducedPolymer);
    }

    private void assertRemoveTypeAndReducePolymer(String polymerString, String typeToRemove, String expectedReducedPolymer) {
        Polymer polymer = new Polymer(polymerString);
        polymer.removeType(typeToRemove);
        assertResultingPolymer(polymer, expectedReducedPolymer);
    }

    private void assertResultingPolymer(Polymer polymer, String expectedReducedPolymer) {
        polymer.reduce();
        assertThat(polymer.polymerString(), is(equalTo(expectedReducedPolymer)));
    }

}


