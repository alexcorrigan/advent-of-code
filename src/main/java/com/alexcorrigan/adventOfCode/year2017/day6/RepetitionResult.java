package com.alexcorrigan.adventOfCode.year2017.day6;

public class RepetitionResult {

    private final Integer[] repeatedMemory;
    private final Integer numberOfDistributionsToReachRepetition;

    public RepetitionResult(Integer[] repeatedMemory, Integer numberOfDistributionsToReachRepetition) {
        this.repeatedMemory = repeatedMemory;
        this.numberOfDistributionsToReachRepetition = numberOfDistributionsToReachRepetition;
    }

    public Integer[] getRepeatedMemory() {
        return repeatedMemory;
    }

    public Integer getNumberOfDistributionsToReachRepetition() {
        return numberOfDistributionsToReachRepetition;
    }

    @Override
    public String toString() {
        StringBuilder memoryString = new StringBuilder();
        for (int m = 0; m < repeatedMemory.length; m++) {
            memoryString.append(repeatedMemory[m]);
            if (m < repeatedMemory.length - 1) {
                memoryString.append(", ");
            }
        }
        return memoryString.toString();
    }

}
