from hamcrest import assert_that, is_

from .solution import Solution
from pytest import fixture


@fixture
def solution() -> Solution:
    yield Solution()


def test_part01(solution):
    assert_that(solution.part_1(), is_("CNSZFDVLJ"))


def test_part02(solution):
    assert_that(solution.part_2(), is_("QNDWLMGNS"))