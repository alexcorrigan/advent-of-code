import os

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(os.path.dirname(__file__))

    def part_1(self, raw_input=None):
        raw_input = self.raw_input if not raw_input else raw_input
        lantern_fishies = LanternFishSchool([int(initial_timer) for initial_timer in raw_input.split(',')])
        lantern_fishies.simulate_days(80)
        return lantern_fishies.sum_fish()

    def part_2(self, raw_input=None):
        raw_input = self.raw_input if not raw_input else raw_input
        lantern_fishies = LanternFishSchool([int(initial_timer) for initial_timer in raw_input.split(',')])
        lantern_fishies.simulate_days(256)
        return lantern_fishies.sum_fish()


class LanternFishSchool(object):

    def __init__(self, initial_timers):
        self.day = 1
        self.fish = {}

        for i in range(0, 9):
            self.fish[i] = 0

        for initial_time in initial_timers:
            self.fish[initial_time] += 1

    def next_day(self):
        reproducing_fish = self.fish[0]

        self.fish[0] = self.fish[1]
        self.fish[1] = self.fish[2]
        self.fish[2] = self.fish[3]
        self.fish[3] = self.fish[4]
        self.fish[4] = self.fish[5]
        self.fish[5] = self.fish[6]
        self.fish[6] = self.fish[7] + reproducing_fish
        self.fish[7] = self.fish[8]
        self.fish[8] = reproducing_fish

    def simulate_days(self, days_ahead):
        while self.day <= days_ahead:
            self.next_day()
            self.day += 1

    def sum_fish(self):
        return sum(self.fish.values())


if __name__ == '__main__':
    solution = Solution()
    input = '3,4,3,1,2'
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
