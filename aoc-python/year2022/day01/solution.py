from os import path
from typing import List, Dict

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        all_food_calories = self.raw_input.splitlines()
        self.elf_calories = self.calculate_elf_calories(all_food_calories)

    def part_1(self):
        return max(self.elf_calories.values())

    def part_2(self):
        return sum(sorted(self.elf_calories.values())[-3:])

    @staticmethod
    def calculate_elf_calories(all_food_calories: List[str]) -> Dict[str, int]:
        elf = 1
        elf_calories = {}
        for food_calories in all_food_calories:
            current_elf = f"elf_{elf}"
            if not food_calories:
                elf += 1
            else:
                food_calories_count = int(food_calories)
                if current_elf not in elf_calories.keys():
                    elf_calories[current_elf] = food_calories_count
                else:
                    elf_calories[current_elf] = elf_calories[current_elf] + food_calories_count
        return elf_calories


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")