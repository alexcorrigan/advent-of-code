package com.alexcorrigan.adventOfCode.year2018.day2;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day2ExampleTest {

    private Scanner scanner;

    @Before
    public void setUp() {
        scanner = new Scanner();
    }

    @Test
    public void part1LetterCountExamples() {
        assertBoxIDScanResult("abcdef", 0, 0);
        assertBoxIDScanResult("bababc", 1, 1);
        assertBoxIDScanResult("abbcde", 1, 0);
        assertBoxIDScanResult("abcccd", 0, 1);
        assertBoxIDScanResult("aabcdd", 2, 0);
        assertBoxIDScanResult("abcdee", 1, 0);
        assertBoxIDScanResult("ababab", 0, 2);
    }

    @Test
    public void part1CheckSumExample() {
        List<BoxID> boxIDs = stream("abcdef,bababc,abbcde,abcccd,aabcdd,abcdee,ababab".split(",")).map(BoxID::new).collect(toList());
        Scanner scanner = new Scanner();
        CheckSum checkSum = scanner.scan(boxIDs);
        assertThat(checkSum.checkSumValue(), is(equalTo(12)));
    }

    @Test
    public void part2Examples() {
        List<BoxID> boxIDs = stream("abcde,fghij,klmno,pqrst,fguij,axcye,wvxyz".split(",")).map(BoxID::new).collect(toList());
        List<BoxIDComparisonResult> boxIDComparisonResultsWithOnlyOneDifference = scanner.findConsecutiveBoxes(boxIDs);
        System.out.println(boxIDComparisonResultsWithOnlyOneDifference);
    }

    private void assertBoxIDScanResult(String boxIDString, int expectedLettersAppearingTwoTimes, int expectedLettersAppearingThreeTimes) {
        BoxID boxID = new BoxID(boxIDString);
        ScanResult scanResult = scanner.scan(boxID);
        assertThat(scanResult.lettersAppearingTwoTimes(), is(equalTo(expectedLettersAppearingTwoTimes)));
        assertThat(scanResult.lettersAppearingThreeTimes(), is(equalTo(expectedLettersAppearingThreeTimes)));
    }

}
