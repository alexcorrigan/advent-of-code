package uk.co.alexcorrigan.adventOfCode.exception.year2023.day02;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GameAnalyser {

    public int analyseGamesWithCriteria(List<String> gameReportStrings, GameHand criteria) {
        List<GameReport> gameReports = gameReportStrings.stream().map(GameReport::fromString).collect(Collectors.toList());
        gameReports.forEach(System.out::println);

        List<Integer> validGames = new ArrayList<>();

        gameReports.forEach(gameReport -> {
            if (gameReport.getMaxRed() <= criteria.getRed() &&
                    gameReport.getMaxGreen() <= criteria.getGreen() &&
                    gameReport.getMaxBlue() <= criteria.getBlue()) {
                validGames.add(gameReport.getId());
            }

        });

        return validGames.stream().mapToInt(a -> a).sum();
    }

    public int analyseGamesForFewestOfEachColour(List<String> gameReportStrings) {
        List<GameReport> gameReports = gameReportStrings.stream().map(GameReport::fromString).collect(Collectors.toList());
        return gameReports.stream().mapToInt(GameReport::getPower).sum();
    }

}
