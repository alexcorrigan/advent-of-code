class Vent(object):

    def __init__(self, raw_coords):
        self.start_x, self.start_y, self.end_x, self.end_y = [int(coord) for coord in
                                                              raw_coords.replace(" -> ", ",").split(",")]
        self.x_direction = 1 if self.start_x <= self.end_x else -1
        self.y_direction = 1 if self.start_y <= self.end_y else -1

    def __str__(self):
        return f"({self.start_x},{self.start_y}) - ({self.end_x},{self.end_y})"