package com.alexcorrigan.adventOfCode.year2018.day3;

public class FabricClaimDimension {

    private final int width;
    private final int height;

    public FabricClaimDimension(String claimDimensionString) {
        this(claimDimensionString.split("x"));
    }

    private FabricClaimDimension(String[] claimDimensionElements) {
        this(Integer.valueOf(claimDimensionElements[0]), Integer.valueOf(claimDimensionElements[1]));
    }

    private FabricClaimDimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("%sx%s", width, height);
    }

    public int width() {
        return width;
    }

    public int height() {
        return height;
    }

}
