package com.alexcorrigan.adventOfCode.year2017.day4;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class Passphrase {

    private final List<String> passphraseElements;

    public static Passphrase passhraseFrom(String passphraseString) {
        return new Passphrase(passphraseString);

    }

    private Passphrase(String passphraseString) {
        passphraseElements = new ArrayList<>(asList(passphraseString.split("\\s")));
    }

    public List<String> getPassphraseElements() {
        return passphraseElements;
    }
}
