package com.alexcorrigan.adventOfCode.year2019.day4;

public class Part2ValidityStrategy implements ValidityStrategy {

    @Override
    public boolean passwordIsValid(Password password) {
        int[] passwordDigits = password.passwordDigits();

        if (passwordDigits.length != 6) {
            return false;
        }

        boolean containsAdjacentPair = false;
        boolean moreThanTwoAdjacent = false;

        for (int i = 0; i < passwordDigits.length - 1; i++) {
            int d1 = passwordDigits[i];
            int d2 = passwordDigits[i + 1];

            if (d2 < d1) {
                return false;
            }

            if (!containsAdjacentPair && d1 == d2) {

                if (i <= passwordDigits.length - 3) {
                    int d3 = passwordDigits[i + 2];
                    if (d2 != d3) {
                        containsAdjacentPair = !moreThanTwoAdjacent;
                    } else {
                        moreThanTwoAdjacent = true;
                    }
                } else {
                    containsAdjacentPair = !moreThanTwoAdjacent;
                }

            } else {
                moreThanTwoAdjacent = false;
            }

        }
        return containsAdjacentPair;
    }

}
