package com.alexcorrigan.adventOfCode.year2019.day5;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_5;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;

public class Day5Test {

    @Before
    public void setUp() throws IOException, URISyntaxException {
        List<String> wirePaths = loadInputData(DAY_5, "2019");
    }

    @Test
    public void testPart1() {
    }

    @Test
    public void testPart2() {
    }

}
