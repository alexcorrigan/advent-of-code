from os import getcwd


class FileUtils:

    @staticmethod
    def load_input_as_integer_list(cwd: str):
        return [int(value) for value in FileUtils.load_input_as_single_string(cwd).splitlines()]

    @staticmethod
    def load_input_as_string_list(cwd: str):
        return FileUtils.load_input_as_single_string(cwd).splitlines()

    @staticmethod
    def load_input_as_single_string(cwd: str) -> str:
        print(getcwd())
        with open(cwd + "/input.txt", 'r') as input_file:
            input_string = input_file.read()
            input_file.close()
        return input_string