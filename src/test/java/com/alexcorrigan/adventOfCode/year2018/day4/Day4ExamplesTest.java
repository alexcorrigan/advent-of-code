package com.alexcorrigan.adventOfCode.year2018.day4;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day4ExamplesTest {

    private List<String> guardRecordStrings;
    private GuardLogAnalyser guardLogAnalyser;
    private Collection<Guard> guards;
    private Strategy strategy;

    @Before
    public void setUp() throws Exception {
        // not chronological order
        guardRecordStrings = new ArrayList<>();
        guardRecordStrings.add("[1518-11-01 00:30] falls asleep");
        guardRecordStrings.add("[1518-11-01 00:55] wakes up");
        guardRecordStrings.add("[1518-11-01 23:58] Guard #99 begins shift");
        guardRecordStrings.add("[1518-11-03 00:29] wakes up");
        guardRecordStrings.add("[1518-11-01 00:05] falls asleep");
        guardRecordStrings.add("[1518-11-01 00:00] Guard #10 begins shift");
        guardRecordStrings.add("[1518-11-01 00:25] wakes up");
        guardRecordStrings.add("[1518-11-04 00:02] Guard #99 begins shift");
        guardRecordStrings.add("[1518-11-03 00:05] Guard #10 begins shift");
        guardRecordStrings.add("[1518-11-03 00:24] falls asleep");
        guardRecordStrings.add("[1518-11-04 00:36] falls asleep");
        guardRecordStrings.add("[1518-11-02 00:50] wakes up");
        guardRecordStrings.add("[1518-11-05 00:55] wakes up");
        guardRecordStrings.add("[1518-11-02 00:40] falls asleep");
        guardRecordStrings.add("[1518-11-05 00:45] falls asleep");
        guardRecordStrings.add("[1518-11-04 00:46] wakes up");
        guardRecordStrings.add("[1518-11-05 00:03] Guard #99 begins shift");

        guardLogAnalyser = new GuardLogAnalyser();
        guards = guardLogAnalyser.buildLog(guardRecordStrings);

        strategy = new Strategy();

    }

    @Test
    public void part1() {
        Guard bestGuard = strategy.strategy1(guards);
        assertThat(bestGuard.toString(), is(equalTo("#10")));
        assertThat(bestGuard.sleepiestMinute(), is(equalTo(24)));
    }

    @Test
    public void part2() {
        Guard bestGuard = strategy.strategy2(guards);
        assertThat(bestGuard.toString(), is(equalTo("#99")));
        assertThat(bestGuard.sleepiestMinute(), is(equalTo(45)));
    }
}
