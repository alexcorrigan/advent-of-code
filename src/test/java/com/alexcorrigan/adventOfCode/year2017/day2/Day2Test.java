package com.alexcorrigan.adventOfCode.year2017.day2;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_2;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day2Test {

    private Spreadsheet spreadsheet;

    @Before
    public void setup() throws IOException, URISyntaxException {
        spreadsheet = new Spreadsheet(loadInputData(DAY_2, "2017"));
    }

    @Test
    public void testPart1SpreadsheetChecksum() throws Exception {
        assertThat(spreadsheet.minMaxChecksum(), is(equalTo(36766)));
    }

    @Test
    public void testPart2SpreadsheetChecksum() throws Exception {
        assertThat(spreadsheet.evenlyDivisibleChecksum(), is(equalTo(261)));
    }
}
