import os
from year2021.day05.vent import Vent
from year2021.day05.ocean_bed import OceanBed
from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self, ):
        super().__init__(os.path.dirname(__file__))

    def part_1(self, ocean_bed: OceanBed, raw_input=None):
        raw_vent_coords = self.raw_input.splitlines() if not raw_input else raw_input.splitlines()
        vents = map(Vent, raw_vent_coords)
        for vent in vents:
            if vent.start_x == vent.end_x or vent.start_y == vent.end_y:
                ocean_bed.plot_straight_vent(vent)
        return ocean_bed.count_vent_intersections()

    def part_2(self, ocean_bed: OceanBed, raw_input=None):
        raw_vent_coords = self.raw_input.splitlines() if not raw_input else raw_input.splitlines()
        vents = map(Vent, raw_vent_coords)
        for vent in vents:
            if vent.start_x == vent.end_x or vent.start_y == vent.end_y:
                ocean_bed.plot_straight_vent(vent)
            else:
                ocean_bed.plot_diagonal_vent(vent)
        return ocean_bed.count_vent_intersections()


if __name__ == '__main__':
    solution = Solution()
    ocean_bed = OceanBed(1000, 1000)
    print(f"Part 1 = {solution.part_1(ocean_bed)}")
    ocean_bed.draw()
    ocean_bed.reset()
    print(f"Part 2 = {solution.part_2(ocean_bed)}")
    ocean_bed.draw()
