package com.alexcorrigan.adventOfCode.year2019.day1;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static com.alexcorrigan.adventOfCode.year2019.day1.Calculations.calculateRequiredFuel;
import static java.math.RoundingMode.DOWN;

public class ModuleMassAndFuelStrategy implements FuelCalculatorStrategy {

    @Override
    public Integer fuelForModuleOfMass(Integer moduleMass) {
        int initialFuelForModule = calculateRequiredFuel(moduleMass);
        int extraFuelForFuel = calculateExtraFuel(initialFuelForModule, 0);
        return initialFuelForModule + extraFuelForFuel;
    }

    private Integer calculateExtraFuel(Integer fuel, Integer totalFuel) {
        int extraFuel = calculateRequiredFuel(fuel);
        if (extraFuel <= 0) {
            return totalFuel;
        } else {
            totalFuel += extraFuel;
            return calculateExtraFuel(extraFuel, totalFuel);
        }
    }

}
