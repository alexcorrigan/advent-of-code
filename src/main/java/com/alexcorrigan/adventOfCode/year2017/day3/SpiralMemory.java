package com.alexcorrigan.adventOfCode.year2017.day3;

public class SpiralMemory {

    public Integer stepsToAccessPortFromMemoryLocation(Integer memoryLocation) {
        if (memoryLocation == 1) {
            return 0;
        } else {
            Double squareRootCeiling = Math.ceil(Math.sqrt(Double.valueOf(memoryLocation)));
            Double curR = squareRootCeiling % 2 != 0 ? squareRootCeiling : squareRootCeiling + 1;
            Double numR = (curR  - 1) / 2;
            Double cycle = memoryLocation - Math.pow(curR - 2, 2);
            Double innerOffset = cycle % (curR - 1);
            Double result = numR + Math.abs(innerOffset - numR);
            return result.intValue();
        }
    }

}
