package com.alexcorrigan.adventOfCode.year2019.day4;

public class PasswordChecker {

    private final ValidityStrategy validityStrategy;

    public PasswordChecker(ValidityStrategy validityStrategy) {
        this.validityStrategy = validityStrategy;
    }

    public boolean isPasswordValid(Password password) {
        return validityStrategy.passwordIsValid(password);
    }

}
