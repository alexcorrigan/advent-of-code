package com.alexcorrigan.adventOfCode.year2018.day4;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_4;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day4Test {

    private Collection<Guard> guards;
    private Strategy strategy;

    @Before
    public void setUp() throws Exception {
        guards = new GuardLogAnalyser().buildLog(loadInputData(DAY_4, "2018"));
        strategy = new Strategy();
    }

    @Test
    public void part1() {
        Guard bestGuard = strategy.strategy1(guards);
        assertThat(bestGuard.toString(), is(equalTo("#971")));
        assertThat(bestGuard.sleepiestMinute(), is(equalTo(38)));
    }

    @Test
    public void part2() {
        Guard bestGuard = strategy.strategy2(guards);
        assertThat(bestGuard.toString(), is(equalTo("#1877")));
        assertThat(bestGuard.sleepiestMinute(), is(equalTo(43)));
    }

}
