def convert_to_intcode(program_string):
    return [int(i) for i in program_string.split(',')]