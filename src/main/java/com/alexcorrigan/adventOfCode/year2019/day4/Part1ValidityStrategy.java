package com.alexcorrigan.adventOfCode.year2019.day4;

public class Part1ValidityStrategy implements ValidityStrategy {

    @Override
    public boolean passwordIsValid(Password password) {
        int[] passwordDigits = password.passwordDigits();
        if (passwordDigits.length != 6) {
            return false;
        }

        boolean containsAdjacentPair = false;

        for (int i = 0; i < passwordDigits.length - 1; i++) {
            int d1 = passwordDigits[i];
            int d2 = passwordDigits[i + 1];

            if (!containsAdjacentPair && d1 == d2) {
                containsAdjacentPair = true;
            }

            if (d2 < d1) {
                return false;
            }
        }
        return containsAdjacentPair;
    }

}
