package com.alexcorrigan.adventOfCode.year2018.day6;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_6;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day6Test {

    private List<String> coordinates;
    private Grid grid;

    @Before
    public void setUp() throws Exception {
        coordinates = loadInputData(DAY_6, "2018");
        grid = new Grid(1000);
    }

    @Test
    public void part1() {
        assertThat(grid.largestNonInfiniteArea(coordinates), is(equalTo(3293)));
    }

    @Test
    public void part2() {
        // agh!!!
        assertThat(grid.otherThing(coordinates), is(equalTo(0)));
    }
}
