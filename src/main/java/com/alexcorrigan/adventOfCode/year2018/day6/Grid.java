package com.alexcorrigan.adventOfCode.year2018.day6;

import java.util.ArrayList;
import java.util.List;

public class Grid {

    private final int[][] grid;


    public Grid(int gridSize) {
        grid = new int[gridSize][gridSize];
    }

    public int largestNonInfiniteArea(List<String> coordinates) {
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid.length; y++) {
                int closest = Integer.MAX_VALUE;
                int coordinatesIndex = 0;
                for (String coordinate : coordinates) {
                    coordinatesIndex++;
                    String[] coordinateElements = coordinate.split(", ");
                    int coordinateX = Integer.valueOf(coordinateElements[0]);
                    int coordinateY = Integer.valueOf(coordinateElements[1]);
                    int distance = mahattenDistance(coordinateX, x, coordinateY, y);

                    if (distance == closest) {
                        grid[x][y] = 0;
                    }

                    if (distance < closest) {
                        closest = distance;
                        grid[x][y] = coordinatesIndex;
                    }

                }
            }
        }

        List<Integer> edges = new ArrayList<>();
        for (int i = 0; i < grid.length; i++) {
            int edge = grid[grid.length - 1][i];
            edges.add(edge);

            edge = grid[i][grid.length - 1];
            edges.add(edge);

            edge = grid[i][0];
            edges.add(edge);

            edge = grid[0][i];
            edges.add(edge);
        }

        int[] countNonInfiniteAreas = new int[coordinates.size()];
        for (int[] gridRow : grid) {
            for (int rowItem : gridRow) {
                if (!edges.contains(rowItem)) {
                    countNonInfiniteAreas[rowItem - 1]++;
                }
            }
        }

        int maxNonInfiniteArea = 0;

        for (int c : countNonInfiniteAreas) {
            if (c > maxNonInfiniteArea) {
                maxNonInfiniteArea = c;
            }
        }

        return maxNonInfiniteArea;
    }

    private int mahattenDistance(int coordinateX, int x, int coordinateY, int y) {
        return Math.abs(x - coordinateX) + Math.abs(y - coordinateY);
    }

    public int otherThing(List<String> coordinates) {
        int inArea = 0;
        for (int x = 0; x < grid.length; x++) {
            for (int y = 0; y < grid.length; y++) {
                int distance = 0;
                for (String coordinate : coordinates) {
                    String[] coordinateElements = coordinate.split(", ");
                    int posX = Integer.parseInt(coordinateElements[0]);
                    int posY = Integer.parseInt(coordinateElements[1]);
                    distance += mahattenDistance(x, posX, y, posY);
                }

                if (distance < 10000) {
                    inArea ++;
                }
            }
        }
        return inArea;
    }
}
