package com.alexcorrigan.adventOfCode.year2017.day1;

public interface CaptchaSolver {

    Integer solveFor(Captcha captcha, CaptchaSolvingStrategy captchaSolvingStrategy) throws Exception;

}
