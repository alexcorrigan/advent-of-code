from hamcrest import *

with open('input.txt', 'r') as raw_map:
    slope_map_original = [row.strip() for row in raw_map.readlines()]
    raw_map.close()

def path_for_slope(slope_map_original, slope):
    slope_map = list(slope_map_original)
    map_width = len(slope_map[0])
    map_height = len(slope_map)

    start = (0,0)
    me = {'x': start[0], 'y': start[1]}

    path = []

    while me['y'] < map_height:
        path.append(slope_map[me['y']][me['x']])
        me['x'] += slope[0]
        me['y'] += slope[1]
        if me['x'] >= map_width:
            for i in range(0, map_height):
               slope_map[i] = slope_map[i] + slope_map_original[i]
               map_width = len(slope_map[0])
    return path

def tree_product_of_slopes(slope_map_original, slopes):
    tree_product = 1

    for slope in slopes:
        trees = trees_in_path(path_for_slope(slope_map_original, slope))
        tree_product = tree_product * trees
    return tree_product


def trees_in_path(path):
    return len([tree for tree in path if tree == '#'])

                
part_1_result = trees_in_path(path_for_slope(slope_map_original, (3,1)))
assert_that(part_1_result, equal_to(218))

# part 2

part_2_result = tree_product_of_slopes(slope_map_original, [(1,1), (3,1), (5,1), (7,1), (1,2)])
assert_that(part_2_result, equal_to(3847183340))

print(f"Part 1 = {part_1_result}")
print(f"Part 2 = {part_2_result}")
    
