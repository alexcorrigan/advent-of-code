package uk.co.alexcorrigan.adventOfCode.exception.year2023.day02;

import static java.util.Objects.requireNonNullElse;

public class GameHand {

    private final Integer red;
    private final Integer green;
    private final Integer blue;

    public GameHand(Integer red, Integer green, Integer blue) {
        this.red = requireNonNullElse(red, 0);
        this.green = requireNonNullElse(green, 0);
        this.blue = requireNonNullElse(blue, 0);
    }

    public Integer getRed() {
        return red;
    }

    public Integer getGreen() {
        return green;
    }

    public Integer getBlue() {
        return blue;
    }

    @Override
    public String toString() {
        return "GameHand{" +
                "red=" + red +
                ", green=" + green +
                ", blue=" + blue +
                '}';
    }

}
