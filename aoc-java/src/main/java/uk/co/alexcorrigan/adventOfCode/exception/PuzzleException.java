package uk.co.alexcorrigan.adventOfCode.exception;

public class PuzzleException extends RuntimeException {

    public PuzzleException() {
        super("Couldn't find solution to puzzle.");
    }

}
