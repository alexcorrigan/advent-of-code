from utils.file_utils import FileUtils


class BaseSolution(object):

    def __init__(self, cwd: str):
        self.raw_input = FileUtils.load_input_as_single_string(cwd)