import pprint
from hamcrest import *
import re

pp = pprint.PrettyPrinter()

with open('input.txt', 'r') as input_file:
    input_lines = [line.strip() for line in input_file.readlines()]
    input_file.close()

raw_test_program = [
    "nop +0", 
    "acc +1", 
    "jmp +4", 
    "acc +3", 
    "jmp -3", 
    "acc -99", 
    "acc +1", 
    "jmp -4", 
    "acc +6"
]

def execute_instruction(accumulator, current_index, operation, argument):
    if operation == "acc":
        accumulator += argument

    current_index += 1 if operation != "jmp" else argument

    return accumulator, current_index


def execute(program):
    accumumulator = 0
    instructions_executed = []
    looped = False
    current_instruction_index = 0

    while current_instruction_index not in instructions_executed:
        instruction = program[current_instruction_index]
        instruction_parts = instruction.split(" ")
        operation = instruction_parts[0]
        argument = int(instruction_parts[1])

        instruction = program[current_instruction_index]
        instructions_executed.append(current_instruction_index)
        accumumulator, current_instruction_index = execute_instruction(accumumulator, current_instruction_index, operation, argument)

    return accumumulator


part_1_test_result = execute(raw_test_program)
assert_that(part_1_test_result, is_(5))
print("Part 1 Test = " + str(part_1_test_result))

part_1_result = execute(input_lines)
assert_that(part_1_result, is_(1563))
print("Part 1 = " + str(part_1_result))

def flip_operation(operation):
    return "nop" if operation == "jmp" else "jmp"
                    
def execute_p2(program):
    instructions_flipped = []

    terminated = False

    while terminated == False:
        accumumulator = 0
        current_instruction_index = 0
        instructions_executed = []
        print(instructions_flipped)
        flipped = False

        while current_instruction_index not in instructions_executed and current_instruction_index < len(program):
            instruction = program[current_instruction_index]
            instruction_parts = instruction.split(" ")
            operation = instruction_parts[0]
            argument = int(instruction_parts[1])

            if flipped == False and operation in ['nop', 'jmp'] and current_instruction_index not in instructions_flipped: 
                operation = flip_operation(operation)
                instructions_flipped.append(current_instruction_index)
                flipped = True

            instructions_executed.append(current_instruction_index)
            accumumulator, current_instruction_index = execute_instruction(accumumulator, current_instruction_index, operation, argument)
            print(accumumulator)


        if current_instruction_index == len(program):
            print("Program terminated")
            terminated = True
        else:
            print("Program began loop")

    return accumumulator


part_2_test_result = execute_p2(raw_test_program)
assert_that(part_2_test_result, is_(8))
print("Part 2 Test = " + str(part_2_test_result))

part_2_result = execute_p2(input_lines)
#assert_that(part_2_result, is_(1563))
print("Part 2 = " + str(part_2_result))
