from os import path

from base_solution import BaseSolution

DAY_1_ACTIONS = {"A": "ROCK", "B": "PAPER", "C": "SCISSORS", "X": "ROCK", "Y": "PAPER", "Z": "SCISSORS"}
DAY_2_TARGET_RESULT = {"X": "LOSE", "Y": "DRAW", "Z": "WIN"}

SHAPE = {
    "ROCK": {
        "beats": "SCISSORS",
        "defeatedBy": "PAPER",
        "points": 1
    },
    "PAPER": {
        "beats": "ROCK",
        "defeatedBy": "SCISSORS",
        "points": 2
    },
    "SCISSORS": {
        "beats": "PAPER",
        "defeatedBy": "ROCK",
        "points": 3
    },
}


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        self.hands = self.raw_input.splitlines()

    def part_1(self):
        score = 0
        wins = 0
        draws = 0
        losses = 0

        for hand in self.hands:
            opponent = DAY_1_ACTIONS[hand[:1].strip()]
            me = DAY_1_ACTIONS[hand[1:].strip()]

            score += SHAPE[me]["points"]

            if me == opponent:
                result = "DRAW"
                score += 3
                draws += 1

            elif SHAPE[me]["beats"] == opponent:
                result = "WIN"
                score += 6
                wins += 1

            else:
                result = "LOSE"
                score += 0
                losses += 1

            print(f"{opponent} vs. {me} = {result}")

        print(f"Wins = {wins}, Draws = {draws}, Losses = {losses}")

        return score

    def part_2(self):
        score = 0
        wins = 0
        draws = 0
        losses = 0
        for hand in self.hands:
            opponent = DAY_1_ACTIONS[hand[:1].strip()]
            target_result = DAY_2_TARGET_RESULT[hand[1:].strip()]

            if target_result == 'LOSE':
                me = SHAPE[opponent]["beats"]
                score += 0 + SHAPE[me]['points']
                losses += 1

            elif target_result == 'DRAW':
                score += 3 + SHAPE[opponent]['points']
                draws += 1

            else:
                me = SHAPE[opponent]["defeatedBy"]
                score += 6 + SHAPE[me]['points']
                wins += 1

        print(f"Wins = {wins}, Draws = {draws}, Losses = {losses}")
        return score


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
