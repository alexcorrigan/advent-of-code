package uk.co.alexcorrigan.adventOfCode.exception.year2023.day02;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GameReport {

    private final Integer id;
    private final List<GameHand> gameHands;
    private final Integer maxRed;
    private final Integer maxGreen;
    private final Integer maxBlue;
    private final Integer power;

    public GameReport(Integer id, List<GameHand> gameInfo) {
        this.id = id;
        this.gameHands = gameInfo;
        this.maxRed = gameHands.stream().max(Comparator.comparing(GameHand::getRed)).orElseThrow().getRed();
        this.maxGreen = gameHands.stream().max(Comparator.comparing(GameHand::getGreen)).orElseThrow().getGreen();
        this.maxBlue = gameHands.stream().max(Comparator.comparing(GameHand::getBlue)).orElseThrow().getBlue();
        this.power = maxRed * maxGreen * maxBlue;
    }

    public static GameReport fromString(String reportString) {
        // Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        String[] game = reportString.split(":");
        Integer id = Integer.valueOf(game[0].split(" ")[1]);
        String[] handStrings = game[1].split(";");

        List<GameHand> gameHands = Stream.of(handStrings).map(handString -> {
            Map<String, Integer> colourCounts = new HashMap<>();
            List.of(handString.split(",")).forEach(handColourString -> {
                String[] handColourStringParts = handColourString.replaceAll("^\\s", "").split(" ");
                colourCounts.put(handColourStringParts[1], Integer.valueOf(handColourStringParts[0]));
            });
            return new GameHand(colourCounts.get("red"), colourCounts.get("green"), colourCounts.get("blue"));
        }).collect(Collectors.toList());

        return new GameReport(id, gameHands);
    }

    public Integer getId() {
        return id;
    }

    public List<GameHand> getGameHands() {
        return gameHands;
    }

    public Integer getMaxRed() {
        return maxRed;
    }

    public Integer getMaxGreen() {
        return maxGreen;
    }

    public Integer getMaxBlue() {
        return maxBlue;
    }

    public Integer getPower() {
        return power;
    }

    @Override
    public String toString() {
        return "GameReport{" +
                "id=" + id +
                ", gameHands=" + gameHands +
                ", maxRed=" + maxRed +
                ", maxGreen=" + maxGreen +
                ", maxBlue=" + maxBlue +
                ", power=" + power +
                '}';
    }
}
