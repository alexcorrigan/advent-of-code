package com.alexcorrigan.adventOfCode.year2019.day1;

import java.math.BigDecimal;

import static java.math.RoundingMode.DOWN;

public class Calculations {

    public static int calculateRequiredFuel(Integer mass) {
        return new BigDecimal(mass).divide(new BigDecimal(3), DOWN).subtract(new BigDecimal(2)).intValue();
    }

}
