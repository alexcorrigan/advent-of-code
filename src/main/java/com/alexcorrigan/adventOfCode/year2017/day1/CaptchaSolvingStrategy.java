package com.alexcorrigan.adventOfCode.year2017.day1;

public interface CaptchaSolvingStrategy {

    int getCaptchaDigitForComparison(Captcha captcha, int baseIndex) throws Exception;
}
