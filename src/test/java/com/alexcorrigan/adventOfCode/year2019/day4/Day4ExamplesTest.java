package com.alexcorrigan.adventOfCode.year2019.day4;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class Day4ExamplesTest {

    @Test
    public void part1Examples() {
        PasswordChecker passwordChecker = new PasswordChecker(new Part1ValidityStrategy());
        assertValidPassword(false, 11, passwordChecker);
        assertValidPassword(true, 111111, passwordChecker);
        assertValidPassword(false, 223450, passwordChecker);
        assertValidPassword(false, 123789, passwordChecker);
        assertValidPassword(true, 123799, passwordChecker);
        assertValidPassword(false, 993799, passwordChecker);
    }

    @Test
    public void part2Examples() {
        PasswordChecker passwordChecker = new PasswordChecker(new Part2ValidityStrategy());
        assertValidPassword(true, 112233, passwordChecker);
        assertValidPassword(false, 123444, passwordChecker);
        assertValidPassword(true, 111122, passwordChecker);

        assertValidPassword(false, 11, passwordChecker);
        assertValidPassword(false, 111111, passwordChecker);
        assertValidPassword(false, 223450, passwordChecker);
        assertValidPassword(false, 123789, passwordChecker);
        assertValidPassword(true, 123799, passwordChecker);
        assertValidPassword(false, 993799, passwordChecker);
        assertValidPassword(false, 111789, passwordChecker);
    }

    private void assertValidPassword(boolean expectedValidity, Integer passwordValue, PasswordChecker passwordChecker) {
        Password password = new Password(passwordValue);
        assertThat(passwordChecker.isPasswordValid(password), is(expectedValidity));
    }

}
