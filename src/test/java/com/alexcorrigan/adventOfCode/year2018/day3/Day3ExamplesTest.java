package com.alexcorrigan.adventOfCode.year2018.day3;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;

public class Day3ExamplesTest {

    private Collection<FabricClaim> fabricClaims;
    private Fabric fabric;

    @Before
    public void setUp() {
        fabricClaims = new ArrayList<>();
        fabricClaims.add(new FabricClaim("#1 @ 1,3: 4x4"));
        fabricClaims.add(new FabricClaim("#2 @ 3,1: 4x4"));
        fabricClaims.add(new FabricClaim("#3 @ 5,5: 2x2"));
        fabric = new Fabric(1000, 1000);
        fabric.addClaims(fabricClaims);
    }

    @Test
    public void part1Examples() {

        List<String> overClaimedSquareInches = fabric.overClaimedSquareInches();
        assertThat(overClaimedSquareInches, hasItem("3,3"));
        assertThat(overClaimedSquareInches, hasItem("3,4"));
        assertThat(overClaimedSquareInches, hasItem("4,3"));
        assertThat(overClaimedSquareInches, hasItem("4,4"));
    }

    @Test
    public void part2Examples() {
        Collection<FabricClaim> overlappingClaims = fabric.overlappingClaims();
        List<FabricClaim> validClaims = new ArrayList<>();
        for (FabricClaim claim : fabricClaims) {
            if (!overlappingClaims.contains(claim)) {
                validClaims.add(claim);
            }
        }
        assertThat(validClaims.size(), is(equalTo(1)));
        assertThat(validClaims.get(0).toString(), is(equalTo("#3 @ 5,5: 2x2")));
    }

}
