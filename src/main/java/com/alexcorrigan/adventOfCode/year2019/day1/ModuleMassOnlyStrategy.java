package com.alexcorrigan.adventOfCode.year2019.day1;

import static com.alexcorrigan.adventOfCode.year2019.day1.Calculations.calculateRequiredFuel;

public class ModuleMassOnlyStrategy implements FuelCalculatorStrategy {

    @Override
    public Integer fuelForModuleOfMass(Integer moduleMass) {
        return calculateRequiredFuel(moduleMass);
    }

}
