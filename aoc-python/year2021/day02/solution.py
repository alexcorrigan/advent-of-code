from os import path

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        self.planned_course = self.raw_input.splitlines()

    def part_1(self):
        horizontal_pos = 0
        depth = 0

        for command_string in self.planned_course:
            command = command_string.split(" ")
            direction = command[0]
            amount = int(command[1])

            if direction == 'forward':
                horizontal_pos += amount

            elif direction == 'up':
                depth -= amount

            elif direction == 'down':
                depth += amount

        return horizontal_pos * depth

    def part_2(self):
        horizontal_pos = 0
        aim = 0
        depth = 0

        for command_string in self.planned_course:
            command = command_string.split(" ")
            direction = command[0]
            amount = int(command[1])

            if direction == 'forward':
                horizontal_pos += amount
                depth += aim * amount

            elif direction == 'up':
                aim -= amount

            elif direction == 'down':
                aim += amount

        return horizontal_pos * depth


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")