package com.alexcorrigan.adventOfCode.year2019.common;

import java.util.ArrayList;
import java.util.List;

public class Input {

    private int nextInputPointer = 0;
    private final List<Integer> inputs = new ArrayList<>();

    public void addInput(Integer value) {
        inputs.add(value);
    }

    public Integer nextInput() {
        Integer nextInput = inputs.get(nextInputPointer);
        nextInputPointer ++;
        return nextInput;
    }

}
