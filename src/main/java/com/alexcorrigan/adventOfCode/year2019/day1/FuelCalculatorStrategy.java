package com.alexcorrigan.adventOfCode.year2019.day1;

public interface FuelCalculatorStrategy {

    Integer fuelForModuleOfMass(Integer moduleMass);

}
