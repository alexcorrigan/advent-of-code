#!/bin/bash

DAYS=25

for DAY in $(seq -f "%02g" 1 $DAYS)
do
	cp -r base_day day${DAY}
done
