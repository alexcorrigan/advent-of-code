package com.alexcorrigan.adventOfCode.year2018.day5;

import com.google.common.primitives.Chars;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;

import static java.lang.Character.*;

public class Polymer {

    private char[] polymerElements;



    public Polymer(String polymerString) {
        this(polymerString.toCharArray());
    }

    public Polymer(char[] polymerElements) {
        this.polymerElements = polymerElements;
    }

    public void reduce() {
        for (int e = 0; e < polymerElements.length - 1; e++) {
            char firstElement = polymerElements[e];
            char secondElement = polymerElements[e +1];
            if (differentPolarities(firstElement, secondElement) && sameType(firstElement, secondElement)) {
                polymerElements = ArrayUtils.remove(polymerElements, e);
                polymerElements = ArrayUtils.remove(polymerElements, e);
                e = -1;
            }
        }
    }

    public void removeType(String type) {
        String polymerString = new String(polymerElements);
        polymerElements = polymerString.replaceAll(type.toLowerCase(), "").replaceAll(type.toUpperCase(), "").toCharArray();
    }

    public String polymerString() {
        return new String(polymerElements);
    }

    private boolean sameType(char firstElement, char secondElement) {
        return toLowerCase(firstElement) == toLowerCase(secondElement);
    }

    private boolean differentPolarities(char first, char second) {
        return !(bothLowerCase(first, second) || bothUpperCase(first, second));
    }

    private boolean bothLowerCase(char first, char second) {
        return isLowerCase(first) && isLowerCase(second);
    }

    private boolean bothUpperCase(char first, char second) {
        return isUpperCase(first) && isUpperCase(second);
    }

}
