#!/bin/bash

YEAR=$1

if [ -z "$YEAR" ]
then
  echo "No year provided!!!"
  exit 1
else
  pytest ./year"${YEAR}"
  exit $?
fi



