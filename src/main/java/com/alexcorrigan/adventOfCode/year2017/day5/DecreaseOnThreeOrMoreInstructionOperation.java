package com.alexcorrigan.adventOfCode.year2017.day5;

public class DecreaseOnThreeOrMoreInstructionOperation implements InstructionOperation {

    @Override
    public void doOperation(Integer step, InstructionSet instructionSet) {
        Integer instructionToOperateOn = instructionSet.getInstructionAt(step);
        Integer newInstruction = instructionToOperateOn >= 3 ? instructionToOperateOn - 1 : instructionToOperateOn + 1;
        instructionSet.overrideInstructionAt(step, newInstruction);
    }

}
