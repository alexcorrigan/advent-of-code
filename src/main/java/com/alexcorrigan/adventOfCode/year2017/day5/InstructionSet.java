package com.alexcorrigan.adventOfCode.year2017.day5;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class InstructionSet {

    private final Integer[] instructions;

    public static InstructionSet instructionSetOf(List<String> rawInstructions) {
        return new InstructionSet(rawInstructions.stream().map(Integer::valueOf).collect(toList()).toArray(new Integer[]{}));
    }

    private InstructionSet(Integer[] instructions) {
        this.instructions = instructions;
    }

    public Integer getInstructionAt(Integer instructionIndex) {
        return instructions[instructionIndex];
    }

    public void overrideInstructionAt(Integer step, Integer newInstruction) {
        instructions[step] = newInstruction;
    }
}
