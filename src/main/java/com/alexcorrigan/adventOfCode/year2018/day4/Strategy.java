package com.alexcorrigan.adventOfCode.year2018.day4;

import java.util.Collection;

public class Strategy {

    public Guard strategy1(Collection<Guard> guards) {
        Guard sleepiestGuard = null;
        int mostMinutesSleeping = 0;
        for (Guard guard : guards) {
            int minutesSleeping = guard.totalMinutesAsleep();
            if (mostMinutesSleeping < minutesSleeping) {
                sleepiestGuard = guard;
                mostMinutesSleeping = minutesSleeping;
            }
        }
        return sleepiestGuard;
    }

    public Guard strategy2(Collection<Guard> guards) {
        Guard mostAsleepGuard = null;
        for (Guard guard : guards) {
            if (mostAsleepGuard == null || mostAsleepGuard.sleepiestMinuteCount() < guard.sleepiestMinuteCount()) {
                mostAsleepGuard = guard;
            }
        }
        return mostAsleepGuard;
    }

}
