package com.alexcorrigan.adventOfCode.year2018.day2;

public class BoxIDComparisonResult {


    private final BoxID firstBoxID;
    private final BoxID secondBoxID;
    private final String commonLetters;
    private final int differences;

    public BoxIDComparisonResult(BoxID firstBoxID, BoxID secondBoxID, String commonLetters, int differences) {
        this.firstBoxID = firstBoxID;
        this.secondBoxID = secondBoxID;
        this.commonLetters = commonLetters;
        this.differences = differences;
    }

    public int differences() {
        return differences;
    }

    @Override
    public String toString() {
        return String.format("%s / %s (%s) -- %s", firstBoxID, secondBoxID, commonLetters, differences);
    }

    public String commonLetters() {
        return commonLetters;
    }

}
