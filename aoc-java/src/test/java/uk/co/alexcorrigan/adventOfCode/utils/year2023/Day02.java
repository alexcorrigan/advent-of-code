package uk.co.alexcorrigan.adventOfCode.utils.year2023;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import uk.co.alexcorrigan.adventOfCode.exception.year2023.day02.GameAnalyser;
import uk.co.alexcorrigan.adventOfCode.exception.year2023.day02.GameHand;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static uk.co.alexcorrigan.adventOfCode.utils.PuzzleInputUtils.readInputAsStrings;

public class Day02 {

    private static final String EXAMPLE_INPUT = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\n" +
            "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\n" +
            "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\n" +
            "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\n" +
            "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    private static GameAnalyser gameAnalyser;
    private static List<String> gameReportStrings;
    private static GameHand criteria;

    @BeforeAll
    public static void setup() throws IOException {
        gameAnalyser = new GameAnalyser();
        gameReportStrings = readInputAsStrings(2023, 2);
        criteria = new GameHand(12, 13, 14);
    }

    @Test
    void part1Example() {
        List<String> gameReports = EXAMPLE_INPUT.lines().collect(Collectors.toList());
        Integer result = gameAnalyser.analyseGamesWithCriteria(gameReports, criteria);
        assertThat(result, equalTo(8));
    }

    @Test
    void part1() {
        Integer result = gameAnalyser.analyseGamesWithCriteria(gameReportStrings, criteria);
        assertThat(result, equalTo(2776));
    }

    @Test
    void part2Example() {
        List<String> gameReports = EXAMPLE_INPUT.lines().collect(Collectors.toList());
        Integer result = gameAnalyser.analyseGamesForFewestOfEachColour(gameReports);
        assertThat(result, equalTo(2286));
    }

    @Test
    void part2() {
        Integer result = gameAnalyser.analyseGamesForFewestOfEachColour(gameReportStrings);
        assertThat(result, equalTo(68638));
    }

}
