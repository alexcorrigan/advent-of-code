package com.alexcorrigan.adventOfCode.year2017.day1;

public class DefaultCaptchaSolver implements CaptchaSolver {

    public Integer solveFor(Captcha captcha, CaptchaSolvingStrategy captchaSolvingStrategy) throws Exception {
        int sum = 0;
        int currentIndex = 0;

        while (currentIndex < captcha.length()) {
            int currentDigit = captcha.getDigit(currentIndex);
            int digitForComparison = captchaSolvingStrategy.getCaptchaDigitForComparison(captcha, currentIndex);
            if (currentDigit == digitForComparison) {
                sum += currentDigit;
            }
            currentIndex ++;
        }
        return sum;
    }

}
