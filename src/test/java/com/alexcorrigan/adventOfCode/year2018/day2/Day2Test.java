package com.alexcorrigan.adventOfCode.year2018.day2;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_2;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day2Test {

    private Scanner scanner;
    private List<BoxID> boxIDs;

    @Before
    public void setUp() throws Exception {
        scanner = new Scanner();
        boxIDs = loadInputData(DAY_2, "2018").stream().map(BoxID::new).collect(toList());
    }

    @Test
    public void part1Test() {
        CheckSum checkSum = scanner.scan(boxIDs);
        assertThat(checkSum.checkSumValue(), is(equalTo(7808)));
    }

    @Test
    public void part2Test() {
        List<BoxIDComparisonResult> boxIDComparisonResultsWithOnlyOneDifference = scanner.findConsecutiveBoxes(boxIDs);
        assertThat(boxIDComparisonResultsWithOnlyOneDifference.size(), is(equalTo(1)));
        assertThat(boxIDComparisonResultsWithOnlyOneDifference.get(0).commonLetters(), is(equalTo("efmyhuckqldtwjyvisipargno")));
        System.out.println(boxIDComparisonResultsWithOnlyOneDifference);
    }

}
