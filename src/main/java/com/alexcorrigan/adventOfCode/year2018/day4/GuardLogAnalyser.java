package com.alexcorrigan.adventOfCode.year2018.day4;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GuardLogAnalyser {

    private final Pattern guardPattern = Pattern.compile("#\\d*");

    public Collection<Guard> buildLog(List<String> rawGuardLog) {
        Map<LocalDateTime, String> chronologicalRawGuardRecords = chronologicallyOrderRecords(rawGuardLog);

        Map<String, Guard> guards = new HashMap<>();

        Guard currentGuard = null;
        LocalTime currentGuardFellAsleep = null;

        for (LocalDateTime dateTime : chronologicalRawGuardRecords.keySet()) {
            String observation = chronologicalRawGuardRecords.get(dateTime);

            if (observation.contains("begins shift")) {
                String guardsName = extractGuard(observation);

                if (!guards.containsKey(guardsName)) {
                    Guard newGuard = new Guard(guardsName);
                    guards.put(guardsName, newGuard);
                    currentGuard = newGuard;
                } else {
                    currentGuard = guards.get(guardsName);
                }

            } else if (observation.contains("falls asleep")) {
                currentGuardFellAsleep = dateTime.toLocalTime();
            } else if (observation.contains("wakes up")) {
                currentGuard.foundSleeping(currentGuardFellAsleep, dateTime.toLocalTime());
            }

        }

        return guards.values();
    }

    private String extractGuard(String observation) {
        Matcher guardMatcher = guardPattern.matcher(observation);
        guardMatcher.find();
        return guardMatcher.group(0);
    }

    private Map<LocalDateTime, String> chronologicallyOrderRecords(List<String> rawGuardLog) {
        Map<LocalDateTime, String> rawGuardRecords = new HashMap<>();
        Pattern datePattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        for (String guardRecordString : rawGuardLog) {
            Matcher dateMatcher = datePattern.matcher(guardRecordString);
            dateMatcher.find();
            String dateTimeString = dateMatcher.group(0);
            LocalDateTime recordDateTime = LocalDateTime.parse(dateTimeString, dateTimeFormatter);
            String recordAction = guardRecordString.replace(String.format("[%s] ", dateTimeString), "");
            rawGuardRecords.put(recordDateTime, recordAction);
        }
        return new TreeMap<>(rawGuardRecords);
    }

}
