package com.alexcorrigan.adventOfCode.year2018.day3;

import java.util.*;

public class Fabric {

    private final Map<String, List<FabricClaim>> claims;

    public Fabric(int width, int height) {
        Map<String, List<FabricClaim>> claims = new HashMap<>();
        for (int w = 0; w < width; w++) {
            for (int h = 0; h < height; h++) {
                claims.put(String.format("%s,%s", w, h), new ArrayList<>());
            }
        }
        this.claims = claims;
    }

    public void addClaim(FabricClaim claim) {
        List<String> claimCoords = claim.allCoordinates();
        for (String claimCoord : claimCoords) {
            claims.get(claimCoord).add(claim);
        }
    }

    public void addClaims(Collection<FabricClaim> claims) {
        claims.forEach(this::addClaim);
    }

    public List<String> claimedSquareInches() {
        List<String> claimedSquareInches = new ArrayList<>();
        for (String squareInch : claims.keySet()) {
            if (claims.get(squareInch).size() > 0) {
                claimedSquareInches.add(squareInch);
            }
        }
        return claimedSquareInches;
    }

    public List<String> overClaimedSquareInches() {
        List<String> claimedSquareInches = new ArrayList<>();
        for (String squareInch : claims.keySet()) {
            if (claims.get(squareInch).size() > 1) {
                claimedSquareInches.add(squareInch);
            }
        }
        return claimedSquareInches;
    }


    public Set<FabricClaim> overlappingClaims() {
        Set<FabricClaim> overlappingClaims = new HashSet<>();
        for (String squareInch : claims.keySet()) {
            if (claims.get(squareInch).size() > 1) {
                overlappingClaims.addAll(claims.get(squareInch));
            }
        }
        return overlappingClaims;
    }

}
