package com.alexcorrigan.adventOfCode.year2019.day4;

public interface ValidityStrategy {

    public boolean passwordIsValid(Password password);

}
