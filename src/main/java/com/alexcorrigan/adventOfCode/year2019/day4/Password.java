package com.alexcorrigan.adventOfCode.year2019.day4;

public class Password {

    private final int[] passwordDigits;

    public Password(int passwordValue) {
        this(Integer.toString(passwordValue).chars().map(c -> c - '0').toArray());
    }

    public Password(int[] passwordDigits) {
        this.passwordDigits = passwordDigits;
    }

    public int[] passwordDigits() {
        return passwordDigits;
    }

    @Override
    public String toString() {
        StringBuilder passwordStringBuilder = new StringBuilder();
        for (int d : passwordDigits) {
            passwordStringBuilder.append(d);
        }
        return passwordStringBuilder.toString();
    }

}
