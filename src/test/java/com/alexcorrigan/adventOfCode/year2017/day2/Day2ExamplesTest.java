package com.alexcorrigan.adventOfCode.year2017.day2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day2ExamplesTest {

    @Test
    public void testPart1Example() throws Exception {
        List<String> rawSpreadSheetData = new ArrayList<>();
        rawSpreadSheetData.add("5 1 9 5");
        rawSpreadSheetData.add("7 5 3");
        rawSpreadSheetData.add("2 4 6 8");
        assertThat(new Spreadsheet(rawSpreadSheetData).minMaxChecksum(), is(equalTo(18)));
    }

    @Test
    public void testPart2Example() throws Exception {
        List<String> rawSpreadSheetData = new ArrayList<>();
        rawSpreadSheetData.add("5 9 2 8");
        rawSpreadSheetData.add("9 4 7 3");
        rawSpreadSheetData.add("3 8 6 5");
        assertThat(new Spreadsheet(rawSpreadSheetData).evenlyDivisibleChecksum(), is(equalTo(9)));
    }
}
