from os import path

from more_itertools import pairwise, sliding_window

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        self.depths = [int(depth) for depth in self.raw_input.splitlines()]

    def part_1(self):
        return sum(b > a for a, b in pairwise(self.depths))

    def part_2(self):
        return sum(sum(b) > sum(a) for a, b in pairwise((sliding_window(self.depths, 3))))


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")