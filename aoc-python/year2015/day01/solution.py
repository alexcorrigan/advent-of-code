import os
from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(os.path.dirname(__file__))

    def part_1(self):
        directions = self.raw_input
        while "()" in directions:
            directions = directions.replace("()", "")

        floor = 0

        for instruction in directions:
            floor = self._next_floor(floor, instruction)
        return floor

    def part_2(self):
        floor = 0
        for index, instruction in enumerate(self.raw_input):
            floor = self._next_floor(floor, instruction)
            if floor < 0:
                return index + 1
        raise Exception('Never entered the basement!')

    @staticmethod
    def _next_floor(current_floor, instruction):
        return current_floor + 1 if instruction == "(" else current_floor - 1


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
