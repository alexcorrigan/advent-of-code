package com.alexcorrigan.adventOfCode.year2019.day4;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class Day4Test {

    private static final int LOWER_BOUND = 367479;
    private static final int UPPER_BOUND = 893698;

    private List<Password> passwords = new ArrayList<>();

    @Before
    public void setUp() {
        for (int p = LOWER_BOUND; p <= UPPER_BOUND; p++) {
            passwords.add(new Password(p));
        }
    }

    @Test
    public void testPart1() {
        PasswordChecker passwordChecker = new PasswordChecker(new Part1ValidityStrategy());
        List<Password> validPasswords = passwords.stream().filter(passwordChecker::isPasswordValid).collect(Collectors.toList());
        assertEquals(495, validPasswords.size());
    }

    @Test
    public void testPart2() {
        PasswordChecker passwordChecker = new PasswordChecker(new Part2ValidityStrategy());
        List<Password> validPasswords = passwords.stream().filter(passwordChecker::isPasswordValid).collect(Collectors.toList());
        assertEquals(305, validPasswords.size());
    }

}
