package com.alexcorrigan.adventOfCode.year2018.day1;

public interface Frequency {
    Frequency change(FrequencyChange frequencyChange);

    Integer toInteger();
}
