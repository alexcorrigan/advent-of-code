
with open('input.txt', 'r') as password_db_file:
    password_db = password_db_file.readlines()
    password_db_file.close()

part_1_test_password_db = ["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"]

def is_valid_part_1(password_record):
    return password_record['x'] <= password_record['password'].count(password_record['letter']) <= password_record['y']

def is_valid_part_2(password_record):
    a = password_record['password'][password_record['x'] - 1]
    b = password_record['password'][password_record['y'] - 1]
    return (a != b) and (password_record['letter'] in [a,b])

def extract_password_record(raw_password_data):
    policy, password = raw_password_data.split(": ")
    occurrences_range, letter = policy.split(" ")
    x, y = occurrences_range.split("-")
    occurrences = password.count(letter)
    return {
        'password': password,
        'letter': letter,
        'x': int(x),
        'y': int(y)
    }

def count_valid_passwords(password_db):
    part_1_valid_passwords = 0
    part_2_valid_passwords = 0
    for raw_password_data in password_db:
        password_record = extract_password_record(raw_password_data)
        if is_valid_part_1(password_record):
            part_1_valid_passwords += 1
        if is_valid_part_2(password_record):
            part_2_valid_passwords += 1
    return part_1_valid_passwords, part_2_valid_passwords


test_valid_passwords = count_valid_passwords(part_1_test_password_db)
valid_passwords = count_valid_passwords(password_db)

print(f"{valid_passwords[0]}, {valid_passwords[1]}")
