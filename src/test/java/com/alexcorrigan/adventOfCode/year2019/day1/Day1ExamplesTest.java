package com.alexcorrigan.adventOfCode.year2019.day1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day1ExamplesTest {

    @Test
    public void part1Examples() {
        FuelCalculator fuelCalculator = new FuelCalculator(new ModuleMassOnlyStrategy());
        assertFuelForModuleWithMass(2, 12, fuelCalculator);
        assertFuelForModuleWithMass(2, 14, fuelCalculator);
        assertFuelForModuleWithMass(654, 1969, fuelCalculator);
        assertFuelForModuleWithMass(33583, 100756, fuelCalculator);
    }

    @Test
    public void part2Examples() {
        FuelCalculator fuelCalculator = new FuelCalculator(new ModuleMassAndFuelStrategy());
        assertFuelForModuleWithMass(2, 12, fuelCalculator);
        assertFuelForModuleWithMass(2, 14, fuelCalculator);
        assertFuelForModuleWithMass(966, 1969, fuelCalculator);
        assertFuelForModuleWithMass(50346, 100756, fuelCalculator);
    }

    private void assertFuelForModuleWithMass(Integer expectedFuel, Integer moduleMass, FuelCalculator fuelCalculator) {
        assertEquals(expectedFuel, fuelCalculator.fuelForModuleMass(moduleMass));
    }

}
