package com.alexcorrigan.adventOfCode.year2018.day3;

import java.util.ArrayList;
import java.util.List;

public class FabricClaim {

    private final int id;
    private final FabricClaimPosition claimPosition;
    private final FabricClaimDimension claimDimension;

    public FabricClaim(String claimString) {
        this(claimString.replace("#", "").replace("@ ", "").replace(":", "").split(" "));
    }

    private FabricClaim(String[] claimStringElements) {
        this(
                Integer.valueOf(claimStringElements[0]),
                new FabricClaimPosition(claimStringElements[1]),
                new FabricClaimDimension(claimStringElements[2])
        );
    }

    private FabricClaim(int id, FabricClaimPosition claimPosition, FabricClaimDimension claimDimension) {
        this.id = id;
        this.claimPosition = claimPosition;
        this.claimDimension = claimDimension;
    }

    @Override
    public String toString() {
        return String.format("#%s @ %s: %s", id, claimPosition, claimDimension);
    }

    public List<String> allCoordinates() {
        List<String> coordinates = new ArrayList<>();
        int inchesFromLeft = claimPosition.inchesFromLeft();
        int inchesFromTop = claimPosition.inchesFromTop();
        for (int i = inchesFromLeft; i < inchesFromLeft + claimDimension.width(); i++) {
            for (int j = inchesFromTop; j < inchesFromTop + claimDimension.height(); j++) {
                coordinates.add(String.format("%s,%s", i, j));
            }
        }
        return coordinates;
    }

}
