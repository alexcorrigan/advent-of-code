from hamcrest import *

with open('input.txt', 'r') as input_file:
    expenses = [int(expense) for expense in input_file.readlines()]
    input_file.close()

for a in expenses:
    for b in expenses:
        if b == a:
            continue
        elif a + b == 2020:
            part_1_result = a*b
            break

for a in expenses:
    for b in expenses:
        if b == a:
            continue
        for c in expenses:
            if c in [a, b]:
                break
            elif a + b + c == 2020:
                part_2_result = a*b*c
                break

assert_that(part_1_result, equal_to(145875))
assert_that(part_2_result, equal_to(69596112))

print("Part 1 = %s" % part_1_result)
print("Part 2 = %s" % part_2_result)
