package com.alexcorrigan.adventOfCode.year2018.day2;

import java.util.List;

public class CheckSumCalculator {

    public CheckSum calculateFrom(List<ScanResult> scanResults) {
        int boxIDsWithLettersAppearingTwoTimes = 0;
        int boxIDsWithLettersAppearingThreeTimes = 0;
        for (ScanResult scanResult : scanResults) {

            if (scanResult.lettersAppearingTwoTimes() > 0) {
                boxIDsWithLettersAppearingTwoTimes += 1;
            }

            if (scanResult.lettersAppearingThreeTimes() > 0) {
                boxIDsWithLettersAppearingThreeTimes += 1;
            }
        }
        return new CheckSum(boxIDsWithLettersAppearingTwoTimes * boxIDsWithLettersAppearingThreeTimes);
    }

}
