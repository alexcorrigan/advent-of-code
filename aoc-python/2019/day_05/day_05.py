from intcode_computer.computer import Computer


def run_program(program):
    computer = Computer(program)
    return computer.execute_intcode_program()


assert str(run_program("1002,4,3,4,33")) == "[1002, 4, 3, 4, 99]"

# PART 1
# enter 1 as input when requested
day_5_program = open("input.txt", 'r').readlines()[0]
run_program(day_5_program)

# Part 1:
#   input = 1, correct answer = 13787043
# Part 2:
#   input = 5, correct answer = 3892695
