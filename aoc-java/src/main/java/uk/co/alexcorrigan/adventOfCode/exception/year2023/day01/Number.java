package uk.co.alexcorrigan.adventOfCode.exception.year2023.day01;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum Number {

    ONE("one", 1),
    TWO("two", 2),
    THREE("three", 3),
    FOUR("four", 4),
    FIVE("five", 5),
    SIX("six", 6),
    SEVEN("seven", 7),
    EIGHT("eight", 8),
    NINE("nine", 9);

    private final String string;
    private final Integer integer;

    Number(String string, Integer integer) {
        this.string = string;
        this.integer = integer;
    }

    public String getString() {
        return string;
    }

    public Integer getInteger() {
        return integer;
    }

    public static List<String> asListOfStrings() {
        return Arrays.stream(values()).map(Number::getString).collect(Collectors.toList());
    }

}
