package com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy;

import com.alexcorrigan.adventOfCode.year2018.day1.Frequency;
import com.alexcorrigan.adventOfCode.year2018.day1.FrequencyChangeSet;

public interface DeviceCalibrationStrategy {

    Frequency calibrate(Frequency initialFrequency, FrequencyChangeSet frequencyChangeSet);

}
