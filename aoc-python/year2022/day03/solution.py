import string
from os import path

from base_solution import BaseSolution
from utils.list_utils import ListUtils


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        self.sacks = self.raw_input.splitlines()
        self.items = {item: priority for priority, item in enumerate(f"{string.ascii_lowercase}{string.ascii_uppercase}", 1)}

    def part_1(self):
        total_priorities = 0
        for sack in self.sacks:
            c1 = sack[:int(len(sack)/2)]
            c2 = sack[int(len(sack)/2):]

            for c1_item in c1:
                if c1_item in c2:
                    total_priorities += self.items[c1_item]
                    break

        return total_priorities

    def part_2(self):
        total_priority = 0
        groups = ListUtils.divide_chunks(self.sacks, 3)
        for group in groups:
            for item in group[0]:
                if item in group[1] and item in group[2]:
                    total_priority += self.items[item]
                    break
        return total_priority


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
