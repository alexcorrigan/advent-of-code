import pprint
from hamcrest import *
import re

pp = pprint.PrettyPrinter()

with open('input.txt', 'r') as input_file:
    input_lines = [line.strip() for line in input_file.readlines()]
    input_file.close()

part_1_result = True
assert_that(part_1_result, is_(True))
print(f"Part 1 = {part_1_result}")

part_2_result = True
assert_that(part_2_result, is_(True))
print(f"Part 2 = {part_2_result}")
