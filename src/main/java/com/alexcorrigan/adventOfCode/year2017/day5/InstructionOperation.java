package com.alexcorrigan.adventOfCode.year2017.day5;

public interface InstructionOperation {
    void doOperation(Integer step, InstructionSet instructionSet);
}
