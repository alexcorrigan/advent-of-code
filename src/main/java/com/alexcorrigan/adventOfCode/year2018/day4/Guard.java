package com.alexcorrigan.adventOfCode.year2018.day4;

import java.time.LocalTime;

import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

public class Guard {

    private final String name;
    private int[] sleepingMinutes = new int[59];

    public Guard(String name) {
        this.name = name;
    }

    public void foundSleeping(LocalTime fellAsleep, LocalTime wokeUp) {
        int minuteFellAsleep = fellAsleep.get(MINUTE_OF_HOUR);
        int minuteWokeUp = wokeUp.get(MINUTE_OF_HOUR);
        for (int m = minuteFellAsleep; m < minuteWokeUp; m++) {
            sleepingMinutes[m] ++;
        }
    }

    public Integer totalMinutesAsleep() {
        int totalMinutes = 0;
        for (int m : sleepingMinutes) {
            totalMinutes += m;
        }
        return totalMinutes;
    }

    public Integer sleepiestMinute() {
        int sleepiestMinute = 0;
        int high = 0;
        for (int m = 0; m < 59; m++) {
            if (high < sleepingMinutes[m]) {
                sleepiestMinute = m;
                high = sleepingMinutes[m];
            }
        }
        return sleepiestMinute;
    }

    public Integer sleepiestMinuteCount() {
        int high = 0;
        for (int m = 0; m < 59; m++) {
            if (high < sleepingMinutes[m]) {
                high = sleepingMinutes[m];
            }
        }
        return high;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }

}
