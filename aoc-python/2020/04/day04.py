import pprint
import re
from hamcrest import *

pp = pprint.PrettyPrinter()

hex_value_pattern = "^#[0-9a-f]{6}$"
id_value_pattern = "^[0-9]{9}$"
height_string_pattern = "^[0-9]*(cm|in)$"

valid_colours = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

mandatory_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']

valid_passport_fields = {
        'byr': {'type': 'year', 'min_year': 1920, 'max_year': 2002},
        'iyr': {'type': 'year', 'min_year': 2010, 'max_year': 2020},
        'eyr': {'type': 'year', 'min_year': 2020, 'max_year': 2030},
        'hgt': {'type': 'height'},
        'hcl': {'type': 'hex_value'},
        'ecl': {'type': 'colour'},
        'pid': {'type': 'id'},
        'cid': {'type': 'optional'}
    }

with open('input.txt', 'r') as input_file:
    batch = [line.strip() for line in input_file.readlines()]
    batch.append("")
    input_file.close()

potential_passports = []
potential_passport = {}

for record in batch:
    if len(record) == 0:
        potential_passports.append(potential_passport)
        potential_passport = {}
    else:
        parameters = record.split(" ")
        for parameter in parameters:
            k,v = parameter.split(":")
            potential_passport[k] = v


def part_1_valid_passports(passport_candidates):
    valid_passports = []

    for passport_candidate in passport_candidates:
        if all(field in passport_candidate for field in mandatory_fields):
            valid_passports.append(passport_candidate)
    return valid_passports

def valid_year(year, min_year, max_year):
    return len(year) == 4 and min_year <= int(year) <= max_year

def valid_height_string(height_string):
    return re.search(height_string_pattern, height_string) != None

def valid_height(height_string):
    valid_units = {
        'cm': {'min_height': 150, 'max_height': 193},
        'in': {'min_height': 59, 'max_height': 76}
    }

    if valid_height_string(height_string):
        unit = height_string[-2:]
        value = int(height_string[:-2])
        return valid_units[unit]['min_height'] <= value <= valid_units[unit]['max_height']

    return False

def valid_hex_value(value):
    return re.search(hex_value_pattern, value) != None

def valid_colour(colour):
    return colour in valid_colours

def part_2_valid_passports(passport_candidates):
    valid_passports = []
    invalid_passports = []

    for passport_candidate in passport_candidates:
        if all(field in passport_candidate for field in mandatory_fields):

            field_checks = {}

            for field in passport_candidate.keys():

                field_value = passport_candidate[field]
                field_to_validate = valid_passport_fields[field]
                field_type = field_to_validate['type']

                if field_type == 'year':
                    field_checks[field] = valid_year(field_value, field_to_validate['min_year'], field_to_validate['max_year'])

                elif field_type == 'height':
                    field_checks[field] = valid_height(field_value)

                elif field_type == 'hex_value':
                    field_checks[field] = valid_hex_value(field_value)

                elif field_type == 'colour':
                    field_checks[field] = valid_colour(field_value)

                elif field_type == 'id':
                    field_checks[field] = re.search(id_value_pattern, field_value) != None

                elif field_type == 'optional':
                    field_checks[field] = True

            if False not in field_checks.values():
                valid_passports.append(passport_candidate)

    return valid_passports

part_1_result = len(part_1_valid_passports(potential_passports))

assert_that(valid_year("2001", 2002, 2004), is_(False))
assert_that(valid_year("2002", 2002, 2004), is_(True))
assert_that(valid_year("2004", 2002, 2004), is_(True))
assert_that(valid_year("2005", 2002, 2004), is_(False))

assert_that(valid_height("cm150"), is_(False))
assert_that(valid_height("in76"), is_(False))

assert_that(valid_height("149cm"), is_(False))
assert_that(valid_height("150cm"), is_(True))
assert_that(valid_height("193cm"), is_(True))
assert_that(valid_height("194cm"), is_(False))

assert_that(valid_height("58in"), is_(False))
assert_that(valid_height("59in"), is_(True))
assert_that(valid_height("76in"), is_(True))
assert_that(valid_height("77in"), is_(False))

assert_that(valid_hex_value("#000000"), is_(True))
assert_that(valid_hex_value("#999999"), is_(True))
assert_that(valid_hex_value("#aaaaaa"), is_(True))
assert_that(valid_hex_value("#ffffff"), is_(True))
assert_that(valid_hex_value("#111aaa"), is_(True))
assert_that(valid_hex_value("#1a1a1a"), is_(True))
assert_that(valid_hex_value("#9c635c"), is_(True))

assert_that(valid_colour("oth"), is_(True))
assert_that(valid_colour("xry"), is_(False))

part_2_result = len(part_2_valid_passports(potential_passports))

assert_that(part_1_result, equal_to(237))
assert_that(part_2_result, equal_to(172))

print("Part 1 = " + str(part_1_result))
print("Part 2 = " + str(part_2_result))
