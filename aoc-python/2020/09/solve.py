import pprint
from hamcrest import *
import re

pp = pprint.PrettyPrinter()

with open('input.txt', 'r') as input_file:
    input_lines = [int(line.strip()) for line in input_file.readlines()]
    input_file.close()

PREAMBLE_LENGTH = 25

test_input_lines = [int(x) for x in ["35", "20", "15", "25", "47", "40", "62", "55", "65", "95", "102", "117", "150", "182", "127", "219", "299", "277", "309", "576"]]

def can_sum(candidate, check_set):
    for a in check_set:
        for b in check_set:
            if a != b:
                if a + b == candidate:
                    return True
    return False
    

def crack(transmission, preamble_length):
    current_candidate_index = preamble_length
    found_weakness = False
    while found_weakness == False and current_candidate_index < len(transmission):
        check_set = transmission[current_candidate_index - preamble_length:current_candidate_index]
        candidate = transmission[current_candidate_index]
        if not can_sum(candidate, check_set):
            return candidate
        current_candidate_index += 1


def contiguous_set(target, transmission):
    start = 0
    collection = []
    found_set = False
    while found_set == False:
        i = start
        total = 0
        while total < target and i < len(transmission):
            total += transmission[i]
            collection.append(transmission[i])
            i += 1

        if total == target:
            found_set = True
        else:
            collection = []
            start += 1

    return collection

    
def find_encryption_weakness(value, transmission):
    result_set = contiguous_set(value, transmission)
    return min(result_set) + max(result_set)

part_1_test_result = crack(test_input_lines, 5)
assert_that(part_1_test_result, is_(127))
print(f"Part 1 Test = {part_1_test_result}")

part_1_result = crack(input_lines, 25)
assert_that(part_1_result, is_(731031916))
print(f"Part 1 = {part_1_result}")

part_2_test_result = find_encryption_weakness(127, test_input_lines)
assert_that(part_2_test_result, is_(62))
print(f"Part 2 Test = {part_2_test_result}")

part_2_result = find_encryption_weakness(731031916, input_lines)
assert_that(part_2_result, is_(93396727))
print(f"Part 2 = {part_2_result}")
