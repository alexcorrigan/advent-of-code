import re
from os import path

from base_solution import BaseSolution
from utils.list_utils import ListUtils

CRATE_STACKS_INPUT_MODE = "crate_stacks"
REARRANGEMENT_PATTERN_INPUT_MODE = "rearrangement_pattern"


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        puzzle_input = self.raw_input.splitlines()
        self.initial_crate_stacks_raw, self.columns, self.rearrangement_pattern_raw = self.parse_input(puzzle_input)

    @staticmethod
    def parse_input(puzzle_input):
        initial_crate_stacks_raw = []
        rearrangement_pattern_raw = []

        input_mode = CRATE_STACKS_INPUT_MODE

        for input_row in puzzle_input:

            if input_row == "":
                input_mode = REARRANGEMENT_PATTERN_INPUT_MODE
            else:
                if input_mode == CRATE_STACKS_INPUT_MODE:
                    initial_crate_stacks_raw.append(input_row)
                else:
                    rearrangement_pattern_raw.append(input_row)
        columns = [int(column) for column in initial_crate_stacks_raw.pop(-1).split()]
        initial_crate_stacks_raw.reverse()
        return initial_crate_stacks_raw, columns, rearrangement_pattern_raw

    @staticmethod
    def build_stacks(initial_crate_stacks_raw, columns):
        stacks = {}

        for raw_crates_at_level in initial_crate_stacks_raw:

            crates_at_level = [crate.replace("[", "").replace("]", "") for crate in ListUtils.divide_chunks(raw_crates_at_level, 4)]

            for column in columns:
                if column not in stacks.keys():
                    stacks[column] = []

                if len(crates_at_level) >= column:
                    if crates_at_level[column - 1] != '   ':
                        stacks[column].append(crates_at_level[column - 1])
        return stacks

    def part_1(self):
        stacks = self.build_stacks(self.initial_crate_stacks_raw, self.columns)

        rearrangement_instructions = []
        for raw_rearrangement_instruction in self.rearrangement_pattern_raw:
            rearrangement_instructions.append(RearrangementInstruction(raw_rearrangement_instruction))

        for rearrangement_instruction in rearrangement_instructions:
            for crates_to_move in range(rearrangement_instruction.quantity):
                crate_being_moved = stacks[rearrangement_instruction.from_stack].pop()
                stacks[rearrangement_instruction.to_stack].append(crate_being_moved)

        top_crates = ""

        for stack in stacks.keys():
            top_crates += stacks[stack][-1]

        return top_crates

    def part_2(self):
        stacks = self.build_stacks(self.initial_crate_stacks_raw, self.columns)

        rearrangement_instructions = []
        for raw_rearrangement_instruction in self.rearrangement_pattern_raw:
            rearrangement_instructions.append(RearrangementInstruction(raw_rearrangement_instruction))

        for rearrangement_instruction in rearrangement_instructions:
            for crates_to_move in range(rearrangement_instruction.quantity, 0, -1):
                crate_being_moved = stacks[rearrangement_instruction.from_stack].pop(-crates_to_move)
                stacks[rearrangement_instruction.to_stack].append(crate_being_moved)
            print(stacks)

        top_crates = ""

        for stack in stacks.keys():
            top_crates += stacks[stack][-1]

        return top_crates


class RearrangementInstruction:

    def __init__(self, raw_instruction: str):
        match = re.search(r"move (\d*) from (\d*) to (\d*)", raw_instruction)
        self.quantity = int(match.group(1))
        self.from_stack = int(match.group(2))
        self.to_stack = int(match.group(3))

    def __repr__(self):
        return f"MOVE {self.quantity} FROM {self.from_stack} TO {self.to_stack}"


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
