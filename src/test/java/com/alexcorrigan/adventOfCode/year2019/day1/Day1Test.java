package com.alexcorrigan.adventOfCode.year2019.day1;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_1;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.junit.Assert.assertEquals;

public class Day1Test {

    private List<Integer> moduleMasses;

    @Before
    public void setUp() throws Exception {
        moduleMasses = loadInputData(DAY_1, "2019").stream().map(Integer::valueOf).collect(Collectors.toList());
    }

    @Test
    public void testPart1() {
        FuelCalculator fuelCalculator = new FuelCalculator(new ModuleMassAndFuelStrategy());
        int totalFuel = calculateTotalFuelRequired(fuelCalculator);
        assertEquals(3210097, totalFuel);
    }

    @Test
    public void testPart2() {
        FuelCalculator fuelCalculator = new FuelCalculator(new ModuleMassAndFuelStrategy());
        int totalFuel = calculateTotalFuelRequired(fuelCalculator);
        assertEquals(4812287, totalFuel);
    }

    private int calculateTotalFuelRequired(FuelCalculator fuelCalculator) {
        int totalFuel = 0;
        for (Integer moduleMass : moduleMasses) {
            totalFuel += fuelCalculator.fuelForModuleMass(moduleMass);
        }
        return totalFuel;
    }

}
