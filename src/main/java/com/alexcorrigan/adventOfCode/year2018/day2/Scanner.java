package com.alexcorrigan.adventOfCode.year2018.day2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scanner {

    private final CheckSumCalculator checkSumCalculator = new CheckSumCalculator();

    public CheckSum scan(List<BoxID> boxIDs) {
        List<ScanResult> scanResults = new ArrayList<>();
        boxIDs.forEach(boxID -> scanResults.add(scan(boxID)));
        return checkSumCalculator.calculateFrom(scanResults);
    }

    public ScanResult scan(BoxID boxID) {
        Map<Character, Integer> letterCounts = new HashMap<>();
        char[] letters = boxID.id().toCharArray();

        for (char letter : letters) {
            int count;
            if (letterCounts.containsKey(letter)) {
                count = letterCounts.get(letter) + 1;
            } else {
                count = 1;
            }
            letterCounts.put(letter, count);
        }

        int lettersAppearingTwoTimes = 0;
        int lettersAppearingThreeTimes = 0;

        for (Character letter : letterCounts.keySet()) {
            int letterCount = letterCounts.get(letter);
            if (letterCount == 2) {
                lettersAppearingTwoTimes += 1;
            } else if (letterCount == 3) {
                lettersAppearingThreeTimes += 1;
            }
        }

        return new ScanResult(lettersAppearingTwoTimes, lettersAppearingThreeTimes);
    }

    public List<BoxIDComparisonResult> findConsecutiveBoxes(List<BoxID> boxIDs) {
        List<BoxIDComparisonResult> boxIDComparisonResultsWithOnlyOneDifference = new ArrayList<>();
        for (int i = 0; i < boxIDs.size(); i++) {
            BoxID currentBoxID = boxIDs.get(i);
            for (int j = i + 1; j < boxIDs.size(); j++) {
                BoxID otherBoxID = boxIDs.get(j);
                BoxIDComparisonResult boxIDComparisonResult = currentBoxID.compareTo(otherBoxID);
                if (boxIDComparisonResult.differences() == 1) {
                    boxIDComparisonResultsWithOnlyOneDifference.add(boxIDComparisonResult);
                }
            }
        }
        return boxIDComparisonResultsWithOnlyOneDifference;
    }

}
