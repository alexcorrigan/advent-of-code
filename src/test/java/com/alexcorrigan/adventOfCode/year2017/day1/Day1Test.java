package com.alexcorrigan.adventOfCode.year2017.day1;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_1;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Day1Test {

    private Captcha captcha;

    @Before
    public void setup() throws IOException, URISyntaxException {
        captcha = new Captcha(loadInputData(DAY_1, "2017").get(0));
    }

    @Test
    public void testPart1Captcha() throws Exception {
        assertThat(new DefaultCaptchaSolver().solveFor(captcha, new NextDigitCaptchaSolvingStrategy()), is(equalTo(1144)));
    }

    @Test
    public void testPart2Captcha() throws Exception {
        assertThat(new DefaultCaptchaSolver().solveFor(captcha, new HalfWayAroundCaptchaSolvingStrategy()), is(equalTo(1194)));
    }

}
