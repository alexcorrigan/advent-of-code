package uk.co.alexcorrigan.adventOfCode.utils.year2023;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import uk.co.alexcorrigan.adventOfCode.exception.year2023.day01.TrebuchetCalibrator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static uk.co.alexcorrigan.adventOfCode.utils.PuzzleInputUtils.readInputAsStrings;

public class Day01 {

    private static final String PART_1_EXAMPLE_INPUT = "1abc2\n" +
            "pqr3stu8vwx\n" +
            "a1b2c3d4e5f\n" +
            "treb7uchet";

    private static final String PART_2_EXAMPLE_INPUT = "two1nine\n" +
            "eightwothree\n" +
            "abcone2threexyz\n" +
            "xtwone3four\n" +
            "4nineeightseven2\n" +
            "zoneight234\n" +
            "7pqrstsixteen";

    private static TrebuchetCalibrator trebuchetCalibrator;
    private static List<String> calibrationValues;

    @BeforeAll
    public static void setup() throws IOException {
        trebuchetCalibrator = new TrebuchetCalibrator();
        calibrationValues = readInputAsStrings(2023, 1);
    }

    @Test
    void part1Example() {
        List<String> calibrationDocument = PART_1_EXAMPLE_INPUT.lines().collect(Collectors.toList());
        Integer result = trebuchetCalibrator.calibrate(calibrationDocument);
        assertThat(result, equalTo(142));
    }

    @Test
    void part1() {
        Integer result = trebuchetCalibrator.calibrate(calibrationValues);
        assertThat(result, equalTo(55607));
    }

    @Test
    void part2Example() {
        List<String> calibrationDocument = PART_2_EXAMPLE_INPUT.lines().collect(Collectors.toList());
        Integer result = trebuchetCalibrator.calibrateV2(calibrationDocument);
        assertThat(result, equalTo(281));
    }

    @Test
    void part2() {
        Integer result = trebuchetCalibrator.calibrateV2(calibrationValues);
        assertThat(result, equalTo(55291));
    }

}
