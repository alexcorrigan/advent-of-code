package com.alexcorrigan.adventOfCode.year2019.day3;

import java.util.List;

import static java.util.Comparator.comparing;

public class WiringReport {

    private final List<IntersectionReport> intersectionReports;

    public WiringReport(List<IntersectionReport> intersectionReports) {
        this.intersectionReports = intersectionReports;
    }

    public IntersectionReport intersectionClosestToCentralPort() {
        return intersectionReports.stream().min(comparing(IntersectionReport::manhattanDistanceFromCentralPort)).get();
    }

    public IntersectionReport intersectionLeastStepsToCentralPort() {
        return intersectionReports.stream().min(comparing(IntersectionReport::combinedStepsFromCentralPort)).get();
    }

}
