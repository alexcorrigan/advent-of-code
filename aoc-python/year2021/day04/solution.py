import os
import re

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(os.path.dirname(__file__))
        input_rows = self.raw_input.splitlines()
        self.numbers = [int(number) for number in input_rows.pop(0).split(",")]
        self.boards = []
        current_board = Board()
        input_rows.pop(0)
        for row in input_rows:
            if row == '':
                self.boards.append(current_board)
                current_board = Board()
            else:
                current_board.append_row(row)

    def part_1(self):
        numbers = self.numbers.copy()

        winner = None
        last_drawn_number = None

        while not winner:
            last_drawn_number = numbers.pop(0)
            for board in self.boards:
                board.mark_number(last_drawn_number)
                if board.got_bingo():
                    winner = board
                    break

        print("First winning board:")
        print(winner)
        return winner.sum_unmarked_numbers() * last_drawn_number

    def part_2(self):
        numbers = self.numbers.copy()

        boards_in_play = self.boards
        last_drawn_number = None
        last_winning_board = None

        while len(boards_in_play) > 0:
            last_drawn_number = numbers.pop(0)
            winning_boards_this_round = self._play_boards(boards_in_play, last_drawn_number)
            boards_in_play = list(set(boards_in_play) - set(winning_boards_this_round))
            if winning_boards_this_round:
                last_winning_board = winning_boards_this_round[-1]

        print("Last winning board:")
        print(last_winning_board)
        return last_winning_board.sum_unmarked_numbers() * last_drawn_number

    @staticmethod
    def _play_boards(boards, drawn_number):
        winning_boards = []
        for board in boards:
            board.mark_number(drawn_number)
            if board.got_bingo():
                winning_boards.append(board)
        return winning_boards


class Board(object):

    MARKED = '=='

    def __init__(self):
        self.number_rows = []

    def append_row(self, row_string):
        self.number_rows.append([int(number) for number in re.split(' +', row_string.strip())])

    def mark_number(self, drawn_number) -> bool:
        got_number = False
        for row_index, row in enumerate(self.number_rows):
            for col_index, board_number in enumerate(row):
                if board_number == drawn_number:
                    self.number_rows[row_index][col_index] = self.MARKED
                    got_number = True
        return got_number

    def got_bingo(self) -> bool:
        for row in self.number_rows:
            if all(cell == self.MARKED for cell in row):
                return True

        for col in range(0, len(self.number_rows)):
            vertical = []
            for row in self.number_rows:
                vertical.append(row[col])
            if all(cell == self.MARKED for cell in vertical):
                return True

        return False

    def sum_unmarked_numbers(self) -> int:
        unmarked_sum = 0
        for row in self.number_rows:
            for cell in row:
                if cell != self.MARKED:
                    unmarked_sum += cell
        return unmarked_sum

    def __str__(self):
        board_string = ''
        for row in self.number_rows:
            row_string = ''
            for number in row:
                row_string += f'{str(number).zfill(2)} '
            board_string += row_string.strip()
            board_string += "\n"
        return board_string


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
