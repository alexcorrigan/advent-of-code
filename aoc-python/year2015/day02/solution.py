import os
from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(os.path.dirname(__file__))

    def part_1(self, raw_input=None):
        presents = raw_input if raw_input else self.raw_input
        presents = presents.splitlines()

        total_area = 0
        for present in presents:
            present_dimensions = present.split("x")

            l = int(present_dimensions[0])
            w = int(present_dimensions[1])
            h = int(present_dimensions[2])

            side_a = l * w
            side_b = w * h
            side_c = h * l
            extra = min(side_a, side_b, side_c)

            total_area += sum([2 * side_a, 2 * side_b, 2 * side_c, extra])
        return total_area

    def part_2(self, raw_input=None):
        presents = raw_input if raw_input else self.raw_input
        presents = presents.splitlines()

        total_length = 0

        for present in presents:
            present_dimensions = [int(dimension) for dimension in present.split("x")]
            present_dimensions.sort()

            length = int(present_dimensions[0])
            width = int(present_dimensions[1])
            height = int(present_dimensions[2])

            shortest_a = int(present_dimensions[0:1][0])
            shortest_b = int(present_dimensions[1:2][0])
            wrap_length = shortest_a + shortest_a + shortest_b + shortest_b

            bow_length = length * width * height
            total_length += wrap_length + bow_length
        return total_length


if __name__ == '__main__':
    solution = Solution()
    print(f'Part 1 = {solution.part_1()}')
    print(f'Part 2 = {solution.part_2()}')
