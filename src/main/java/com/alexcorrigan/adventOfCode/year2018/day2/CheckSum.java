package com.alexcorrigan.adventOfCode.year2018.day2;

public class CheckSum {

    private final int checkSumValue;

    public CheckSum(int checkSumValue) {
        this.checkSumValue = checkSumValue;
    }

    public int checkSumValue() {
        return checkSumValue;
    }

}
