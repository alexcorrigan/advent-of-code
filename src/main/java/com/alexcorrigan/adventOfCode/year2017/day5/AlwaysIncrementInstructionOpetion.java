package com.alexcorrigan.adventOfCode.year2017.day5;

public class AlwaysIncrementInstructionOpetion implements InstructionOperation {

    @Override
    public void doOperation(Integer step, InstructionSet instructionSet) {
        Integer newInstruction = instructionSet.getInstructionAt(step) + 1;
        instructionSet.overrideInstructionAt(step, newInstruction);
    }

}
