
ORIGIN = (0, 0)
DIRECTIONS = {'R': (1, 0), 'L': (-1, 0), 'U': (0, 1), 'D': (0, -1)}


class Wire:

    def __init__(self, wire_path_string):
        self._vectors = ((DIRECTIONS[instruction[0]], int(instruction[1:])) for instruction in wire_path_string.split(","))
        self.plot = self._plot_from()

    def _plot_from(self):
        plot = []
        current_point = ORIGIN
        for vector in self._vectors:
            for step in range(vector[1]):
                next_point = (current_point[0] + vector[0][0], current_point[1] + vector[0][1])
                plot.append(next_point)
                current_point = next_point
        return plot


def wire_intersections(wire_plots):
    return set(wire_plots[0]) & set(wire_plots[1])


def day_3(wire_strings):
    wire_plots = [Wire(wire_string).plot for wire_string in wire_strings]
    intersections = wire_intersections(wire_plots)
    distance_to_closest_intersection = min(abs(intersection[0]) + abs(intersection[1]) for intersection in intersections)
    fewest_combined_steps_to_an_intersection = min(sum(wire_plot.index(intersection) for wire_plot in wire_plots) + len(wire_strings) for intersection in intersections)
    return distance_to_closest_intersection, fewest_combined_steps_to_an_intersection


assert day_3(["R8,U5,L5,D3", "U7,R6,D4,L4"]) == (6, 30)
assert day_3(["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"]) == (159, 610)
assert day_3(["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"]) == (135, 410)

wire_paths = open("input.txt", 'r').readlines()
assert day_3([wire_paths[0], wire_paths[1]]) == (529, 20386)