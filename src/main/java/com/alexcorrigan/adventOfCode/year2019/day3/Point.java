package com.alexcorrigan.adventOfCode.year2019.day3;

public class Point {

    private final Integer x;
    private final Integer y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int manhattanDistanceFromOrigin() {
        return Math.abs(x) + Math.abs(y);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Point) {
            Point that = (Point) o;
            return x.equals(that.x) && y.equals(that.y);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x.hashCode();
        result = prime * result + y.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

}
