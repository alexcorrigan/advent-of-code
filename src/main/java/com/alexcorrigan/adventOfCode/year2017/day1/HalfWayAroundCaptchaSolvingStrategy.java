package com.alexcorrigan.adventOfCode.year2017.day1;

public class HalfWayAroundCaptchaSolvingStrategy implements CaptchaSolvingStrategy {
    public int getCaptchaDigitForComparison(Captcha captcha, int baseIndex) throws Exception {
        return captcha.getDigitHalfWayAround(baseIndex);
    }
}
