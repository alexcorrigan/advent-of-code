package com.alexcorrigan.adventOfCode.year2019.day2;

import com.alexcorrigan.adventOfCode.year2019.common.Computer;
import org.junit.Before;
import org.junit.Test;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_2;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.junit.Assert.assertEquals;

public class Day2Test {

    private Computer computer;

    @Before
    public void setUp() throws Exception {
        String gravityAssistProgramString = loadInputData(DAY_2, "2019").get(0);
        computer = new Computer(gravityAssistProgramString);
    }

    @Test
    public void testPart1() throws Exception {
        computer.overrideMemoryAtAddress(1, 12);
        computer.overrideMemoryAtAddress(2, 2);
        computer.execute();
        assertEquals(3765464, computer.valueAtMemoryAddress(0));
    }

    @Test
    public void testPart2() throws Exception {
        int foundNoun = -1;
        int foundVerb = -1;
        for (int noun = 0; noun < 100; noun ++) {
            for (int verb = 0; verb < 100; verb++) {
                computer.reset();
                computer.overrideMemoryAtAddress(1, noun);
                computer.overrideMemoryAtAddress(2, verb);
                computer.execute();
                int output = computer.valueAtMemoryAddress(0);
                if (output == 19690720) {
                    foundNoun = noun;
                    foundVerb = verb;
                    break;
                }
            }
        }
        int answer = 100 * foundNoun + foundVerb;
        assertEquals(7610, answer);
    }

}
