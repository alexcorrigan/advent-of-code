from PIL import Image, ImageDraw
from year2021.day05.vent import Vent


class OceanBed(object):
    PLAIN_BED = '.'

    def __init__(self, width, length):
        self.width = width
        self.length = length
        self.grid = []
        self.reset()

    def reset(self):
        self.grid = []
        for row in range(0, self.width):
            self.grid.append([])
            for cell in range(0, self.length):
                self.grid[row].append(self.PLAIN_BED)

    def plot_straight_vent(self, vent: Vent):
        for col in range(vent.start_x, vent.end_x + vent.x_direction, vent.x_direction):
            for row in range(vent.start_y, vent.end_y + vent.y_direction, vent.y_direction):
                if self.grid[row][col] == self.PLAIN_BED:
                    self.grid[row][col] = 1
                else:
                    self.grid[row][col] += 1

    def plot_diagonal_vent(self, vent: Vent):
        current_x = vent.start_x - vent.x_direction
        current_y = vent.start_y - vent.y_direction
        while current_x != vent.end_x and current_y != vent.end_y:
            current_x += vent.x_direction
            current_y += vent.y_direction
            if self.grid[current_y][current_x] == self.PLAIN_BED:
                self.grid[current_y][current_x] = 1
            else:
                self.grid[current_y][current_x] += 1

    def count_vent_intersections(self):
        intersection_count = 0
        for row in self.grid:
            for cell in row:
                if cell != self.PLAIN_BED and cell > 1:
                    intersection_count += 1
        return intersection_count

    def draw(self):
        # Do a drawing !!! :-)
        canvas = Image.new(mode="RGB", size=(1000, 1000), color=(15, 15, 35))
        draw = ImageDraw.Draw(canvas)
        for r_index, row in enumerate(self.grid):
            for c_index, cell in enumerate(row):
                if cell != self.PLAIN_BED:
                    draw.point((c_index, r_index), fill=(0, 75 * cell, 0))
        canvas.show()

    def __str__(self):
        ocean_bed_string = ''
        for row in self.grid:
            for cell in row:
                ocean_bed_string += str(cell)
            ocean_bed_string += '\n'
        return ocean_bed_string
