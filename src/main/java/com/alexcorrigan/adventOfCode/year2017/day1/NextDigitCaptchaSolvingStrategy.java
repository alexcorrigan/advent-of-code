package com.alexcorrigan.adventOfCode.year2017.day1;

public class NextDigitCaptchaSolvingStrategy implements CaptchaSolvingStrategy {

    public int getCaptchaDigitForComparison(Captcha captcha, int baseIndex) {
        return captcha.getNextDigit(baseIndex);
    }

}
