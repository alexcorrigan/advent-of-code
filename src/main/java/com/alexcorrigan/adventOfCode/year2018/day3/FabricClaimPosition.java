package com.alexcorrigan.adventOfCode.year2018.day3;

public class FabricClaimPosition {

    private final int inchesFromLeft;
    private final int inchesFromTop;


    public FabricClaimPosition(String positionString) {
        this(positionString.split(","));
    }

    private FabricClaimPosition(String[] positionStringElements) {
        this(Integer.valueOf(positionStringElements[0]), Integer.valueOf(positionStringElements[1]));
    }

    private FabricClaimPosition(int inchesFromLeft, int inchesFromTop) {
        this.inchesFromLeft = inchesFromLeft;
        this.inchesFromTop = inchesFromTop;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", inchesFromLeft, inchesFromTop);
    }

    public int inchesFromLeft() {
        return inchesFromLeft;
    }

    public int inchesFromTop() {
        return inchesFromTop;
    }

}
