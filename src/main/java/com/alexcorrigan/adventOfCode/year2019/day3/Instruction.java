package com.alexcorrigan.adventOfCode.year2019.day3;

import static java.lang.Integer.parseInt;
import static org.apache.commons.lang3.StringUtils.substring;

public class Instruction {

    private final String direction;
    private final int distance;

    public Instruction(String instructionString) {
        this(substring(instructionString, 0, 1), parseInt(substring(instructionString, 1)));
    }

    public Instruction(String direction, int distance) {
        this.direction = direction;
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "Instruction{" +
                "direction='" + direction + '\'' +
                ", distance=" + distance +
                '}';
    }

    public boolean isLeft() {
        return direction.equals("L");
    }

    public boolean isRight() {
        return direction.equals("R");
    }

    public boolean isUp() {
        return direction.equals("U");
    }

    public boolean isDown() {
        return direction.equals("D");
    }

    public int distance() {
        return distance;
    }

    public String direction() {
        return direction;
    }
}
