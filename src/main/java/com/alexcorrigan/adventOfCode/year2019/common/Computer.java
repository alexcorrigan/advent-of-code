package com.alexcorrigan.adventOfCode.year2019.common;

import static com.alexcorrigan.adventOfCode.year2019.common.ComputerUtils.parseProgram;

public class Computer {

    private final int[] program;
    private int[] memory;
    private Input input;

    public Computer(String programString) {
        this(parseProgram(programString), new Input());
    }

    public Computer(String programString, Input input) {
        this(parseProgram(programString), input);
    }

    public Computer(int[] program, Input input) {
        this.program = program.clone();
        memory = program.clone();
        this.input = input;
    }

    public void reset() {
        memory = program.clone();
    }

    public int[] execute() throws Exception {
        int instructionPointer = 0;
        boolean halt = false;

        while (!halt) {

            int opcode = memory[instructionPointer];

            if (opcodeIsArithmetic(opcode)) {
                int parameterA = memory[instructionPointer + 1];
                int parameterB = memory[instructionPointer + 2];
                int resultAddress = memory[instructionPointer + 3];

                switch (opcode) {
                    case 1:
                        overrideMemoryAtAddress(resultAddress, valueAtMemoryAddress(parameterA) + valueAtMemoryAddress(parameterB));
                        break;
                    case 2:
                        overrideMemoryAtAddress(resultAddress, valueAtMemoryAddress(parameterA) * valueAtMemoryAddress(parameterB));
                        break;
                }

                instructionPointer += 4;

            } else if (opcode == 3) {
                int parameter = memory[instructionPointer + 1];
                overrideMemoryAtAddress(parameter, input.nextInput());
                instructionPointer += 2;

            } else if (opcode == 4) {
                int parameter = memory[instructionPointer + 1];
                System.out.println(memory[parameter]);
                instructionPointer += 2;


            } else if (opcodeIsHalt(opcode)) {
                halt = true;
                System.out.println("halting");

            } else {
                throw new Exception("Unrecognised opcode: " + memory[instructionPointer]);
            }
        }

        return memory;
    }

    private boolean opcodeIsHalt(int opcode) {
        return opcode == 99;
    }

    private boolean opcodeIsArithmetic(int opcode) {
        return opcode == 1 | opcode == 2;
    }

    public int valueAtMemoryAddress(int address) {
        return memory[address];
    }

    public void overrideMemoryAtAddress(int address, int newValue) {
        memory[address] = newValue;
    }
}
