package com.alexcorrigan.adventOfCode.year2017.day4;

public interface ValidityStrategy {

    boolean isValid(Passphrase passphrase);

}
