package com.alexcorrigan.adventOfCode.year2017.day5;

import org.junit.Before;
import org.junit.Test;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_5;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static com.alexcorrigan.adventOfCode.year2017.day5.InstructionSet.instructionSetOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day5Test {

    private Jumper jumper;
    private InstructionSet instructionSet;

    @Before
    public void setUp() throws Exception {
        jumper = new Jumper();
        instructionSet = instructionSetOf(loadInputData(DAY_5, "2017"));
    }

    @Test
    public void day5Part1() throws Exception {
        assertThat(jumper.followInstructions(instructionSet, new AlwaysIncrementInstructionOpetion()), is(equalTo(381680)));
    }

    @Test
    public void day5Part2() throws Exception {
        assertThat(jumper.followInstructions(instructionSet, new DecreaseOnThreeOrMoreInstructionOperation()), is(equalTo(29717847)));
    }
}
