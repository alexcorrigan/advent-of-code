package com.alexcorrigan.adventOfCode.year2018.day3;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_3;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day3Test {


    private List<FabricClaim> claims;
    private Fabric fabric;

    @Before
    public void setUp() throws Exception {
        claims = loadInputData(DAY_3, "2018").stream().map(FabricClaim::new).collect(toList());
        fabric = new Fabric(1000, 1000);
        fabric.addClaims(claims);
    }

    @Test
    public void part1() {
        List<String> overClaimedSquareInches = fabric.overClaimedSquareInches();
        assertThat(overClaimedSquareInches.size(), is(equalTo(110546)));
    }

    @Test
    public void part2() {
        Collection<FabricClaim> overlappingClaims = fabric.overlappingClaims();
        List<FabricClaim> validClaims = new ArrayList<>();
        for (FabricClaim claim : claims) {
            if (!overlappingClaims.contains(claim)) {
                validClaims.add(claim);
            }
        }
        assertThat(validClaims.size(), is(Matchers.equalTo(1)));
        assertThat(validClaims.get(0).toString(), is(Matchers.equalTo("#819 @ 882,366: 17x20")));
    }

}
