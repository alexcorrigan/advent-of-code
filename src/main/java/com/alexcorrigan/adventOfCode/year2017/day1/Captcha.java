package com.alexcorrigan.adventOfCode.year2017.day1;

public class Captcha {

    private final String captchaString;
    private final char[] captchaChars;

    public Captcha(String captchaString) {
        this.captchaString = captchaString;
        captchaChars = captchaString.toCharArray();
    }

    public Integer getDigit(Integer index) {
        return  Integer.valueOf(Character.toString(captchaChars[index]));
    }

    public Integer getNextDigit(int index) {
        return getDigit((index + 1) % captchaChars.length);
    }

    public Integer getDigitHalfWayAround(int index) throws Exception {
        if (captchaChars.length % 2 == 0) {
            return getDigit(((captchaChars.length / 2) + index) % captchaChars.length);
        } else {
            throw new Exception("Captcha length isn't even.");
        }
    }

    public Integer length() {
        return captchaChars.length;
    }

    @Override
    public boolean equals(Object obj) {
        return obj != null && obj instanceof Captcha && ((Captcha) obj).captchaString.equals(this.captchaString);
    }
}
