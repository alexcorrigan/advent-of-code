package com.alexcorrigan.adventOfCode.common;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class InputLoader {

    public static List<String> loadInputData(DayOfAdvent dayOfAdvent, String year) throws URISyntaxException, IOException {
        URL input = ClassLoader.getSystemResource("year" + year + "/" + dayOfAdvent.getDayOfAdventString() + "/" + dayOfAdvent.getDayOfAdventString() + "_input.txt");
        return Files.readAllLines(Paths.get(input.toURI()));
    }

}
