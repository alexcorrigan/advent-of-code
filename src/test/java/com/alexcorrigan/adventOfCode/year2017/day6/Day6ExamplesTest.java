package com.alexcorrigan.adventOfCode.year2017.day6;

import org.junit.Before;
import org.junit.Test;

import static com.alexcorrigan.adventOfCode.year2017.day6.MemoryBank.memoryBankFrom;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Day6ExamplesTest {

    private static final String EXAMPLE_MEMORY_BANK_CONFIGURATION = "0\t2\t7\t0";

    private MemoryBank memoryBank;

    @Before
    public void setUp() throws Exception {
        memoryBank = memoryBankFrom(EXAMPLE_MEMORY_BANK_CONFIGURATION.split("\t"));
    }

    @Test
    public void part1Example() throws Exception {
        assertThat(memoryBank.determineNumberOfRedistributionsBeforeRepetition().getNumberOfDistributionsToReachRepetition(), is(equalTo(5)));
    }

    @Test
    public void part2Example() throws Exception {
        RepetitionResult repetitionResult = memoryBank.determineNumberOfRedistributionsBeforeRepetition();
        MemoryBank newMemoryBank = memoryBankFrom(repetitionResult.getRepeatedMemory());
        assertThat(newMemoryBank.determineNumberOfRedistributionsBeforeRepetition().getNumberOfDistributionsToReachRepetition(), is(equalTo(4)));
    }

}
