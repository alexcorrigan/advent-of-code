package com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy;

import com.alexcorrigan.adventOfCode.year2018.day1.Frequency;
import com.alexcorrigan.adventOfCode.year2018.day1.FrequencyChange;
import com.alexcorrigan.adventOfCode.year2018.day1.FrequencyChangeSet;

public class SingleFrequencyDeviceCalibrationStrategy implements DeviceCalibrationStrategy {

    @Override
    public Frequency calibrate(Frequency initialFrequency, FrequencyChangeSet frequencyChangeSet) {
        System.out.println("Calibrating...");
        Frequency currentFrequency = initialFrequency;
        System.out.println(String.format("- initial frequency = %s", currentFrequency));
        for (FrequencyChange frequencyChange : frequencyChangeSet) {
            System.out.println(String.format("- applying frequency change = %s", frequencyChange));
            currentFrequency = currentFrequency.change(frequencyChange);
            System.out.println(String.format("- current frequency = %s", currentFrequency));
        }
        return currentFrequency;
    }
}
