import math
import pprint
from hamcrest import *

pp = pprint.PrettyPrinter()

rows = 0,127
cols = 0,7

with open('input.txt', 'r') as input_file:
    input_lines = [line.strip() for line in input_file.readlines()]
    input_file.close()

def partition_reduce(range, partition):
    lower_partition = range[0], range[0] + int(math.floor((range[1] - range[0]) / 2))
    upper_partition = range[0] + int(math.ceil((range[1] - range[0]) / 2)), range[1]
    return lower_partition if partition in ["F", "L"] else upper_partition

assert_that(partition_reduce((0,127), "F"), equal_to((0,63))) 
assert_that(partition_reduce((0,63), "B"), equal_to((32,63))) 
assert_that(partition_reduce((32,63), "F"), equal_to((32,47))) 
assert_that(partition_reduce((32,47), "B"), equal_to((40,47))) 
assert_that(partition_reduce((40,47), "B"), equal_to((44,47))) 
assert_that(partition_reduce((44,47), "F"), equal_to((44,45))) 
assert_that(partition_reduce((44,45), "F"), equal_to((44,44))) 

assert_that(partition_reduce((0,7), "R"), equal_to((4,7)))
assert_that(partition_reduce((4,7), "L"), equal_to((4,5)))
assert_that(partition_reduce((4,5), "R"), equal_to((5,5)))

def locate_partition(partition_string, space):
    for partition in partition_string:
        space = partition_reduce(space, partition)
    assert space[0] == space[1]
    return space[0]

assert_that(locate_partition('FBFBBFF', rows), equal_to(44))
assert_that(locate_partition('RLR', cols), equal_to(5))

def calculate_seat_id(row, col):
    return (row * 8) + col

def locate_seat(boarding_pass):
    row_location = boarding_pass[:7]
    col_location = boarding_pass[-3:]
    row = locate_partition(row_location, rows)
    col = locate_partition(col_location, cols)
    return row, col, calculate_seat_id(row, col)


assert_that(locate_seat('FBFBBFFRLR'), equal_to((44,5,357)))
assert_that(locate_seat('BFFFBBFRRR'), equal_to((70,7,567)))
assert_that(locate_seat('FFFBBBFRRR'), equal_to((14,7,119)))
assert_that(locate_seat('BBFFBBFRLL'), equal_to((102,4,820)))

seat_ids = []

for boarding_pass in input_lines:
    seat = locate_seat(boarding_pass)
    seat_ids.append(seat[2])

part_1_result = max(seat_ids)
assert_that(part_1_result, is_(878))
print("Part 1 = " + str(part_1_result))

seat_ids.sort()

for seat_id in seat_ids:
    seat_id_index = seat_ids.index(seat_id)
    if seat_id_index > 0 and seat_id - 1 != seat_ids[seat_id_index - 1]:
        seat_before = seat_ids[seat_id_index - 1]
        seat_after = seat_ids[seat_id_index]

assert_that(seat_before, is_(503))
assert_that(seat_after, is_(505))

print("Part 2 = betweeen %s and %s" % (seat_before, seat_after))

