package com.alexcorrigan.adventOfCode.year2018.day1;

import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.DeviceCalibrationStrategy;
import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.DuplicateFrequencyDeviceCalibrationStrategy;
import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.SingleFrequencyDeviceCalibrationStrategy;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_1;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Day1Test {

    private FrequencyChangeSet frequencyChangeSet;

    @Before
    public void setUp() throws Exception {
        List<String> frequencyChanges = loadInputData(DAY_1, "2018");
        frequencyChangeSet = new FrequencyChangeSet();
        frequencyChangeSet.addAll(frequencyChanges);
    }

    @Test
    public void testPart1() throws Exception {
        assertDeviceCalibration(new SingleFrequencyDeviceCalibrationStrategy(), 484);
    }

    @Test
    public void testPart2() throws Exception {
        assertDeviceCalibration(new DuplicateFrequencyDeviceCalibrationStrategy(), 367);
    }

    private void assertDeviceCalibration(DeviceCalibrationStrategy deviceCalibrationStrategy, Integer expectedCalibratedFrequency) {
        Device device = new Device(deviceCalibrationStrategy);
        device.calibrate(frequencyChangeSet, new SimpleFrequency(0));
        assertThat(device.calibratedFrequency().toInteger(), is(equalTo(expectedCalibratedFrequency)));
    }

}
