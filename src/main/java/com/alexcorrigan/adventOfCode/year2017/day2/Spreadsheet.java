package com.alexcorrigan.adventOfCode.year2017.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

public class Spreadsheet {

    private final List<List<Integer>> spreadsheetData = new ArrayList<>();

    public Spreadsheet(List<String> rawSpreadsheetData) {
        rawSpreadsheetData.forEach((String row) -> {
            List<Integer> rowData = new ArrayList<>();
            asList(row.split("\\s")).forEach(data -> rowData.add(Integer.valueOf(data)));
            spreadsheetData.add(rowData);
        });
    }

    public Integer minMaxChecksum() {
        return spreadsheetData.stream().mapToInt(row -> Collections.max(row) - Collections.min(row)).sum();
    }

    public Integer evenlyDivisibleChecksum() {
        return spreadsheetData.stream().mapToInt(this::findQuotientOfOnlyEvenlyDivisiblePairOnRow).sum();
    }

    private Integer findQuotientOfOnlyEvenlyDivisiblePairOnRow(List<Integer> row) {
        for (int i = 0; i < row.size(); i++) {
            for (int j = 0; j < row.size(); j++) {
                if (i != j) {
                    int a = row.get(i);
                    int b = row.get(j);
                    if (a % b == 0) {
                        return a/b;
                    } else if (b % a == 0) {
                        return b/a;
                    }
                }
            }
        }
        return null;
    }

}
