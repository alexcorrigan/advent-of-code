package com.alexcorrigan.adventOfCode.year2017.day4;

import java.util.Arrays;
import java.util.List;

public class NoDuplicatedAnagramsValidityStrategy implements ValidityStrategy {

    @Override
    public boolean isValid(Passphrase passphrase) {
        List<String> passphraseElements = passphrase.getPassphraseElements();

        for (int i = 0; i < passphraseElements.size(); i++) {
            for (int j = 0; j < passphraseElements.size(); j++) {
                if (i != j) {
                    String elementAOrdered = orderStringCharactersAlphabetically(passphraseElements.get(i));
                    String elementBOrdered = orderStringCharactersAlphabetically(passphraseElements.get(j));
                    if (elementAOrdered.equals(elementBOrdered)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private String orderStringCharactersAlphabetically(String a) {
        char[] stringChars = a.toCharArray();
        Arrays.sort(stringChars);
        return new String(stringChars);
    }

}
