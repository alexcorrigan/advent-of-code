package uk.co.alexcorrigan.adventOfCode.utils;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

public class PuzzleInputUtils {

    private static final String PUZZLE_INPUT_ROOT = "puzzleInputs";

    public static List<Integer> readInputAsInts(int year, int day) throws IOException {
        return readInputs(year, day).stream().map(Integer::valueOf).collect(Collectors.toList());
    }

    public static List<String> readInputAsStrings(final int year, final int day) throws IOException {
        return readInputs(year, day);
    }

    private static List<String> readInputs(final int year, final int day) throws IOException {
        File inputFile = new File(String.format("%s/%s/%s.txt", PUZZLE_INPUT_ROOT, year, StringUtils.leftPad(String.valueOf(day), 2, "0")));
        return Files.readAllLines(inputFile.toPath(), Charset.defaultCharset());
    }

}
