package com.alexcorrigan.adventOfCode.year2018.day1;

import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.DeviceCalibrationStrategy;
import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.DuplicateFrequencyDeviceCalibrationStrategy;
import com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy.SingleFrequencyDeviceCalibrationStrategy;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day1ExamplesTest {

    @Test
    public void part1Examples() throws Exception {
        assertPart1DeviceCalibratedFrequency(3, "+1,-2,+3,+1");
        assertPart1DeviceCalibratedFrequency(3, "+1,+1,+1");
        assertPart1DeviceCalibratedFrequency(0, "+1,+1,-2");
        assertPart1DeviceCalibratedFrequency(-6, "-1,-2,-3");
    }

    @Test
    public void part2Examples() throws Exception {
        assertPart2DeviceCalibratedFrequency(2, "+1,-2,+3,+1");
        assertPart2DeviceCalibratedFrequency(0, "+1,-1");
        assertPart2DeviceCalibratedFrequency(10, "+3,+3,+4,-2,-4");
        assertPart2DeviceCalibratedFrequency(5, "-6,+3,+8,+5,-6");
        assertPart2DeviceCalibratedFrequency(14, "+7,+7,-2,-7,-4");
    }

    private void assertPart1DeviceCalibratedFrequency(Integer expectedCalibratedFrequency, String frequencyChanges) {
        assertDeviceCalibratedFrequency(new SingleFrequencyDeviceCalibrationStrategy(), expectedCalibratedFrequency, frequencyChanges);
    }

    private void assertPart2DeviceCalibratedFrequency(Integer expectedCalibratedFrequency, String frequencyChanges) {
        assertDeviceCalibratedFrequency(new DuplicateFrequencyDeviceCalibrationStrategy(), expectedCalibratedFrequency, frequencyChanges);
    }

    private void assertDeviceCalibratedFrequency(DeviceCalibrationStrategy deviceCalibrationStrategy, Integer expectedCalibratedFrequency, String frequencyChanges) {
        Device device = new Device(deviceCalibrationStrategy);
        FrequencyChangeSet frequencyChangeSet = new FrequencyChangeSet();
        frequencyChangeSet.addAll(frequencyChanges);
        device.calibrate(frequencyChangeSet, new SimpleFrequency(0));
        assertEquals(expectedCalibratedFrequency, device.calibratedFrequency().toInteger());
    }

}
