import pprint

pp = pprint.PrettyPrinter(indent=2)

CENTER_OF_MASS = 'COM'
YOU = 'YOU'
SANTA = 'SAN'

part_1_test = ['COM)B', 'B)C', 'C)D', 'E)F', 'D)E', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L']
part_2_test = ['COM)B', 'B)C', 'C)D', 'E)F', 'D)E', 'B)G', 'G)H', 'D)I', 'E)J', 'J)K', 'K)L', 'K)YOU', 'I)SAN']

with open('input.txt', 'r') as input_file:
    day6_orbit_map_list = input_file.readlines()
    input_file.close()


def from_list(map_list):
    map = {}
    for pair in map_list:
        elements = pair.split(')')
        mass = elements[0].strip()
        satelite = elements[1].strip()
        map[satelite] = mass
    return map


def count_indirect_orbits_to_com(count, map, satelite):
    base = map[satelite]
    if base == CENTER_OF_MASS:
        return count
    else:
        count += 1
        return count_indirect_orbits_to_com(count, map, base)


def total_orbits_for_map(orbit_map):
    total_direct_orbits = len(orbit_map.keys())
    total_indirect_orbits = 0

    for satelite in orbit_map.keys():
        indirect_orbits = count_indirect_orbits_to_com(0, orbit_map, satelite)
        total_indirect_orbits += indirect_orbits

    return total_direct_orbits + total_indirect_orbits


def orbit_hops(orbit_map):
    you_path_to_com = []

    current_mass = orbit_map[YOU]
    while current_mass != CENTER_OF_MASS:
        next_mass = orbit_map[current_mass]
        you_path_to_com.append(next_mass)
        current_mass = next_mass

    current_mass = orbit_map[SANTA]
    path_intersect = None
    santa_hops_to_intersect = 0
    while not path_intersect:
        if current_mass in you_path_to_com:
            path_intersect = current_mass
        else:
            santa_hops_to_intersect += 1
            current_mass = orbit_map[current_mass]
    return you_path_to_com.index(path_intersect) + 1 + santa_hops_to_intersect


pp.pprint("test map total orbits = " + str(total_orbits_for_map(from_list(part_1_test))))

day6_part1_result = total_orbits_for_map(from_list(day6_orbit_map_list))
pp.pprint("day 6 part 1 map total orbits = " + str(day6_part1_result))
assert day6_part1_result == 294191

day6_part2_test_result = orbit_hops(from_list(part_2_test))
assert day6_part2_test_result == 4

day_6_part2 = orbit_hops(from_list(day6_orbit_map_list))
print("day 6 part 2 map total orbits = " + str(day_6_part2))

