package com.alexcorrigan.adventOfCode.year2019.day2;

import com.alexcorrigan.adventOfCode.year2019.common.Computer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day2ExamplesTest {

    @Test
    public void part1Examples() throws Exception {
        assertComputerOutput("2,0,0,0,99", "1,0,0,0,99");
        assertComputerOutput("2,3,0,6,99", "2,3,0,3,99");
        assertComputerOutput("2,4,4,5,99,9801", "2,4,4,5,99,0");
        assertComputerOutput("30,1,1,4,2,5,6,0,99", "1,1,1,4,99,5,6,0,99");
    }

    @Test
    public void part2Examples() {

    }

    private void assertComputerOutput(String expectedOutput, String programString) throws Exception {
        Computer computer = new Computer(programString);
        assertEquals(expectedOutput, computerOutputToString(computer.execute()));
    }

    private String computerOutputToString(int[] computerOutput) {
        StringBuilder computerOutputStringBuilder = new StringBuilder();
        for (int computerOutputValue : computerOutput) {
            computerOutputStringBuilder.append(computerOutputValue).append(",");
        }
        computerOutputStringBuilder.deleteCharAt(computerOutputStringBuilder.length() - 1);
        return computerOutputStringBuilder.toString();
    }

}
