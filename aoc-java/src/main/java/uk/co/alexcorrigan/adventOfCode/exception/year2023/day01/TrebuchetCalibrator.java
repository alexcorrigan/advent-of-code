package uk.co.alexcorrigan.adventOfCode.exception.year2023.day01;

import java.util.List;

public class TrebuchetCalibrator {

    public Integer calibrate(List<String> calibrationDocument) {
        return calibrationDocument.stream().mapToInt(value -> {
            String firstDigit = String.valueOf(findFirstInteger(value, false));
            String lastDigit = String.valueOf(findFirstInteger(value, true));

            String calibrationValue = String.format("%s%s", firstDigit, lastDigit);
            System.out.printf("Value: %s -- First: %s, Last: %s, Calibration Value: %s\n", value, firstDigit, lastDigit, calibrationValue);
            return Integer.parseInt(calibrationValue);
        }).sum();
    }

    public Integer calibrateV2(List<String> calibrationDocument) {
        return calibrationDocument.stream().mapToInt(value -> {
            String firstDigit = String.valueOf(findFirstIntegerOrNumberString(value, false));
            String lastDigit = String.valueOf(findFirstIntegerOrNumberString(value, true));

            String calibrationValue = String.format("%s%s", firstDigit, lastDigit);
            System.out.printf("Value: %s -- First: %s, Last: %s, Calibration Value: %s\n", value, firstDigit, lastDigit, calibrationValue);
            return Integer.parseInt(calibrationValue);
        }).sum();
    }

    private Integer findFirstInteger(String value, Boolean reverseDirection) {
        if (reverseDirection) {
            value = reverseString(value);
        }

        for (Character character : value.toCharArray()) {
            if (isInteger(character)) {
                return Integer.valueOf(character.toString());
            }
        }
        return null;
    }

    private Integer findFirstIntegerOrNumberString(String value, Boolean reverseDirection) {
        String possibleNumberString = "";

        if (reverseDirection) {
            value = reverseString(value);
        }

        for (Character character : value.toCharArray()) {
            if (isInteger(character)) {
                return Integer.valueOf(character.toString());
            } else {

                if (!reverseDirection) {
                    possibleNumberString += character;
                } else {
                    possibleNumberString = character + possibleNumberString;
                }

                Integer integer = extractIntegerFromNumberString(possibleNumberString);
                if (integer != null) {
                    return integer;
                }
            }
        }
        return null;
    }

    private Boolean isInteger(Character character) {
        try {
            Integer.valueOf(character.toString());
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    private Integer extractIntegerFromNumberString(String possibleNumberString) {
        for (String numberString : Number.asListOfStrings()) {
            if (possibleNumberString.contains(numberString)) {
                return Number.valueOf(numberString.toUpperCase()).getInteger();
            }
        }
        return null;
    }

    private String reverseString(String string) {
        return new StringBuilder(string).reverse().toString();
    }

}
