package com.alexcorrigan.adventOfCode.year2018.day1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class FrequencyChangeSet implements Iterable<FrequencyChange> {

    private final List<FrequencyChange> frequencyChanges = new ArrayList<>();

    public void addAll(String commaSeparatedFrequencyChanges) {
        addAll(Arrays.asList(commaSeparatedFrequencyChanges.split(",")));
    }

    public void addAll(List<String> frequencyChanges) {
        for (String frequencyChangeString : frequencyChanges) {
            this.frequencyChanges.add(new FrequencyChange(frequencyChangeString));
        }
    }

    @Override
    public Iterator<FrequencyChange> iterator() {
        return frequencyChanges.iterator();
    }

}
