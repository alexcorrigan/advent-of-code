package com.alexcorrigan.adventOfCode.year2018.day2;

public class ScanResult {

    private final int lettersAppearingTwoTimes;
    private final int lettersAppearingThreeTimes;

    public ScanResult(int lettersAppearingTwoTimes, int lettersAppearingThreeTimes) {
        this.lettersAppearingTwoTimes = lettersAppearingTwoTimes;
        this.lettersAppearingThreeTimes = lettersAppearingThreeTimes;
    }

    public int lettersAppearingTwoTimes() {
        return lettersAppearingTwoTimes;
    }

    public int lettersAppearingThreeTimes() {
        return lettersAppearingThreeTimes;
    }

}
