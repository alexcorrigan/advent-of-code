import pprint
from hamcrest import *
import re

pp = pprint.PrettyPrinter()

MY_BAG = "shiny gold"

with open('input.txt', 'r') as input_file:
    input_lines = [line.strip() for line in input_file.readlines()]
    input_file.close()

bag_rules = {}

for line in input_lines:
    bag_colour = re.search("^(.*) bags contain ", line)[1]
    bag_contents = {}
    if "contain no other bags." in line:
        pass
    else:
        matches = re.findall("(((\d) ([a-z\s]*)) (bags|bag))", line)
        for match in matches:
            bag_contents[match[3]] = int(match[2])
        
    bag_rules[bag_colour] = bag_contents

assert_that(bag_rules.keys(), has_length(594))

def outside_bags(root_bag, bags, bags_holding_root_bag):
    for bag in bags.keys():
        if root_bag in bags[bag].keys():
            bags_holding_root_bag.add(bag)
            outside_bags(bag, bags, bags_holding_root_bag)
    return bags_holding_root_bag

bags_that_container_at_least_one = outside_bags(MY_BAG, bag_rules, set())

assert_that(bags_that_container_at_least_one, has_length(148))
print("Part 1 = " + str(len(bags_that_container_at_least_one)))

def collect_inside_bags(root_bag, bags, collected_bags):
    for bag in bags[root_bag].keys():
        for i in range(bags[root_bag][bag]):
            collected_bags.append(bag)
            collect_inside_bags(bag, bags, collected_bags)
    return collected_bags

inside_bags = collect_inside_bags(MY_BAG, bag_rules, [])

assert_that(inside_bags, has_length(24867))
print("Part 2 = " + str(len(inside_bags)))
