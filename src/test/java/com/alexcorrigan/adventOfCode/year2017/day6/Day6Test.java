package com.alexcorrigan.adventOfCode.year2017.day6;

import org.junit.Before;
import org.junit.Test;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_6;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Day6Test {

    private MemoryBank memoryBank;

    @Before
    public void setUp() throws Exception {
        memoryBank = MemoryBank.memoryBankFrom(loadInputData(DAY_6, "2017").get(0).split("\t"));
    }

    @Test
    public void part1() throws Exception {
        assertThat(memoryBank.determineNumberOfRedistributionsBeforeRepetition(), is(equalTo(11137)));
    }

    @Test
    public void part2() throws Exception {
        RepetitionResult repetitionResult = memoryBank.determineNumberOfRedistributionsBeforeRepetition();
        MemoryBank newMemoryBank = MemoryBank.memoryBankFrom(repetitionResult.getRepeatedMemory());
        assertThat(newMemoryBank.determineNumberOfRedistributionsBeforeRepetition().getNumberOfDistributionsToReachRepetition(), is(equalTo(1037)));
    }

}
