package com.alexcorrigan.adventOfCode.year2018.day1;

public class NullFrequency implements Frequency {

    @Override
    public Frequency change(FrequencyChange frequencyChange) {
        return new SimpleFrequency(frequencyChange.applyTo(0));
    }

    @Override
    public Integer toInteger() {
        return null;
    }

    @Override
    public String toString() {
        return "NULL";
    }
}
