package com.alexcorrigan.adventOfCode.year2017.day5;

public class Jumper {

    public int followInstructions(InstructionSet instructionSet, InstructionOperation instructionOperation) {
        int instructionsCompletedInSteps = 0;
        Integer currentStep = 0;

        IndexOutOfBoundsException indexOutOfBoundsException = null;

        while (indexOutOfBoundsException == null) {
            try {
                Integer currentInstruction = instructionSet.getInstructionAt(currentStep);
                instructionOperation.doOperation(currentStep, instructionSet);
                instructionsCompletedInSteps++;
                currentStep += currentInstruction;
            } catch (IndexOutOfBoundsException e) {
                indexOutOfBoundsException = e;
            }
        }

        return instructionsCompletedInSteps;
    }

}
