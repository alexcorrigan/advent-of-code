package com.alexcorrigan.adventOfCode.year2018.day5;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_5;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class Day5Test {

    private final char[] types = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    private Map<Character, Integer> removeAndReducePolymerLengths = new HashMap<>();

    private String polymerString;

    @Before
    public void setUp() throws Exception {
        polymerString = loadInputData(DAY_5, "2018").get(0);
    }

    @Test
    public void part1() {
        Polymer polymer = new Polymer(polymerString);
        polymer.reduce();
        System.out.println(polymer.polymerString());
        assertThat(polymer.polymerString().length(), is(equalTo(9370)));
    }

    @Test
    public void part2() {
        for (char typeToRemove : types) {
            Polymer polymer = new Polymer(polymerString);
            polymer.removeType("" + typeToRemove);
            polymer.reduce();
            removeAndReducePolymerLengths.put(typeToRemove, polymer.polymerString().length());
        }

        Character bestTypeToRemoveFirst = null;
        int bestLength = Integer.MAX_VALUE;

        for (Character typeRemoved : removeAndReducePolymerLengths.keySet()) {
            if (bestTypeToRemoveFirst == null) {
                bestTypeToRemoveFirst = typeRemoved;
            } else {
                int lengthAfterRemovingType = removeAndReducePolymerLengths.get(typeRemoved);
                if (lengthAfterRemovingType < bestLength) {
                    bestTypeToRemoveFirst = typeRemoved;
                    bestLength = lengthAfterRemovingType;
                }
            }
        }
        assertThat(bestTypeToRemoveFirst, is(equalTo('d')));
        assertThat(bestLength, is(equalTo(6390)));
    }
}
