package com.alexcorrigan.adventOfCode.year2017.day5;

import org.junit.Before;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Day5ExamplesTest {

    private final Jumper jumper = new Jumper();
    private InstructionSet instructionSet;

    @Before
    public void setUp() throws Exception {
        instructionSet = InstructionSet.instructionSetOf(asList("0\n3\n0\n1\n-3".split("\n")));
    }

    @Test
    public void part1Example() throws Exception {
        assertThat(jumper.followInstructions(instructionSet, new AlwaysIncrementInstructionOpetion()), is(equalTo(5)));
    }

    @Test
    public void part2Example() throws Exception {
        assertThat(jumper.followInstructions(instructionSet, new DecreaseOnThreeOrMoreInstructionOperation()), is(equalTo(10)));
    }

}
