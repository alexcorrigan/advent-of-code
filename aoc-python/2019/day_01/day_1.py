import math

module_masses = open('input.txt', 'r').readlines()


def fuel_for_mass(mass):
    return int(math.floor(int(mass) / 3)) - 2


def part_1_total_fuel_for_modules(module_masses):
    total_fuel = 0
    for module_mass in module_masses:
        total_fuel += fuel_for_mass(module_mass)
    return total_fuel


def part_2_total_fuel_for_module(mass, current_fuel_allocation):
    fuel_for_this_mass = fuel_for_mass(mass)
    if fuel_for_this_mass <= 0:
        return current_fuel_allocation
    else:
        return part_2_total_fuel_for_module(fuel_for_this_mass, current_fuel_allocation + fuel_for_this_mass)


def part_2_total_fuel_for_modules(module_masses):
    total_fuel_for_all_modules = 0
    for module_mass in module_masses:
        total_fuel_for_all_modules += part_2_total_fuel_for_module(module_mass, 0)
    return total_fuel_for_all_modules


assert part_1_total_fuel_for_modules([12]) == 2
assert part_1_total_fuel_for_modules([14]) == 2
assert part_1_total_fuel_for_modules([1969]) == 654

assert part_2_total_fuel_for_modules([14]) == 2
assert part_2_total_fuel_for_modules([1969]) == 966
assert part_2_total_fuel_for_modules([100756]) == 50346

print("Part 1: %s" % part_1_total_fuel_for_modules(module_masses))
print("Part 2: %s" % part_2_total_fuel_for_modules(module_masses))
