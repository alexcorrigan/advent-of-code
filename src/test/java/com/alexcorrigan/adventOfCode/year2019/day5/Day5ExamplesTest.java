package com.alexcorrigan.adventOfCode.year2019.day5;

import com.alexcorrigan.adventOfCode.year2019.common.Computer;
import com.alexcorrigan.adventOfCode.year2019.common.Input;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Day5ExamplesTest {

    @Test
    public void opcode3Test() throws Exception {
        Input input = new Input();
        input.addInput(1);
        Computer computer = new Computer("3,0,1,0,0,0,99", input);
        assertComputerOutput(computer, "2,0,1,0,0,0,99");
    }

    @Test
    public void opcode4Test() throws Exception {
        Computer computer = new Computer("4,0,99");
        assertComputerOutput(computer, "4,0,99");
    }

    @Test
    public void part1Examples() {

    }

    @Test
    public void part2Examples() {
    }

    private void assertComputerOutput(Computer computer, String expectedOutput) throws Exception {
        assertEquals(expectedOutput, computerOutputToString(computer.execute()));
    }

    private String computerOutputToString(int[] computerOutput) {
        StringBuilder computerOutputStringBuilder = new StringBuilder();
        for (int computerOutputValue : computerOutput) {
            computerOutputStringBuilder.append(computerOutputValue).append(",");
        }
        computerOutputStringBuilder.deleteCharAt(computerOutputStringBuilder.length() - 1);
        return computerOutputStringBuilder.toString();
    }

}
