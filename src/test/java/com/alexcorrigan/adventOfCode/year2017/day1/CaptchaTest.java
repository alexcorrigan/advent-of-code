package com.alexcorrigan.adventOfCode.year2017.day1;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CaptchaTest {

    private static final Captcha ODD_LENGTH_CAPTURE = new Captcha("12345");
    private static final Captcha EVEN_LENGTH_CAPTURE = new Captcha("1234");

    @Test
    public void testGetSpecificDigitFromCaptcha() throws Exception {
        assertThat(1, is(equalTo(ODD_LENGTH_CAPTURE.getDigit(0))));
        assertThat(5, is(equalTo(ODD_LENGTH_CAPTURE.getDigit(4))));
    }

    @Test
    public void testGetNextDigitInCaptcha() throws Exception {
        assertThat(ODD_LENGTH_CAPTURE.getNextDigit(0), is(equalTo(2)));
        assertThat(ODD_LENGTH_CAPTURE.getNextDigit(4), is(equalTo(1)));
    }

    @Test
    public void testGetDigitHalfWayAround() throws Exception {
        assertThat(EVEN_LENGTH_CAPTURE.getDigitHalfWayAround(0), is(equalTo(3)));
        assertThat(EVEN_LENGTH_CAPTURE.getDigitHalfWayAround(3), is(equalTo(2)));
    }
}
