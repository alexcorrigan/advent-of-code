from hamcrest import assert_that, is_
from pytest import fixture
from .ocean_bed import OceanBed
from .solution import Solution


@fixture
def solution() -> Solution:
    yield Solution()


def test_part01(solution):
    ocean_bed = OceanBed(1000, 1000)
    assert_that(solution.part_1(ocean_bed), is_(7644))


def test_part02(solution):
    ocean_bed = OceanBed(1000, 1000)
    assert_that(solution.part_2(ocean_bed), is_(18627))