package com.alexcorrigan.adventOfCode.year2017.day4;

import org.junit.Test;

import static com.alexcorrigan.adventOfCode.year2017.day4.Passphrase.passhraseFrom;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day4ExamplesTest {

    @Test
    public void testPart1Examples() throws Exception {
        PassphraseValidityChecker passphraseValidityChecker = new PassphraseValidityChecker();
        ValidityStrategy validityStrategy = new NoDuplicatedWordsValidityStrategy();
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("aa bb cc dd ee"), validityStrategy), is(true));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("aa bb cc dd aa"), validityStrategy), is(false));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("aa bb cc dd aaa"), validityStrategy), is(true));
    }

    @Test
    public void testPart2Examples() throws Exception {
        PassphraseValidityChecker passphraseValidityChecker = new PassphraseValidityChecker();
        ValidityStrategy validityStrategy = new NoDuplicatedAnagramsValidityStrategy();
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("abcde fghij"), validityStrategy), is(true));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("abcde xyz ecdab"), validityStrategy), is(false));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("a ab abc abd abf abj"), validityStrategy), is(true));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("iiii oiii ooii oooi oooo"), validityStrategy), is(true));
        assertThat(passphraseValidityChecker.isValid(passhraseFrom("oiii ioii iioi iiio"), validityStrategy), is(false));
    }

}
