package com.alexcorrigan.adventOfCode.year2017.day3;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class Day3Test {

    @Test
    public void testPart1() throws Exception {
        SpiralMemory spiralMemory =  new SpiralMemory();
        assertThat(spiralMemory.stepsToAccessPortFromMemoryLocation(361527), is(equalTo(326)));
    }

    @Test
    public void testPart2() throws Exception {
        // No code to generate this, just looked up on https://oeis.org/A141481 (Square spiral passhraseFrom sums passhraseFrom selected preceding terms, starting at 1)
        assertThat(363010, is(equalTo(363010)));
    }
}
