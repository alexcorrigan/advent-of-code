import pprint
from hamcrest import *

pp = pprint.PrettyPrinter()

with open('input.txt', 'r') as input_file:
    input_lines = [line.strip() for line in input_file.readlines()]
    input_lines.append("")
    input_file.close()

groups = []
group = []

for line in input_lines:
    if len(line) == 0:
        groups.append(group)
        group = []
    else:
        group.append(line)

question_counts = 0
questions_everyone_answered = 0

for group in groups:
    questions = {}
    group_size = len(group)

    for member, positive_questions in enumerate(group):
        for question in positive_questions:
            if question in questions:
                questions[question].append(member)
            else:
                questions[question] = [member]

    question_counts += len(questions.keys())
    
    questions_everyone_in_group_answered = []
    for question in questions.keys():
        if len(questions[question]) == group_size:
            questions_everyone_in_group_answered.append(question)

    questions_everyone_answered += len(questions_everyone_in_group_answered)
         

assert_that(question_counts, equal_to(6903))
print("Part 1 = " + str(question_counts))

assert_that(questions_everyone_answered, equal_to(3493))
print("Part 2 = " + str(questions_everyone_answered))

