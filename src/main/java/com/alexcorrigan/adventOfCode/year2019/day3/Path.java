package com.alexcorrigan.adventOfCode.year2019.day3;

import java.util.*;
import java.util.stream.Collectors;

public class Path {

    private final List<Instruction> instructions;

    public Path(String pathString) {
        this(Arrays.stream(pathString.split(",")).map(Instruction::new).collect(Collectors.toList()));
    }

    public Path(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    public Map<Point, Integer> traversePath() {
        Map<Point, Integer> visitedPoints = new HashMap<>();

        int currentX = 0;
        int currentY = 0;
        int steps = 0;

        for (Instruction instruction : instructions) {
            for (int move = 1; move <= instruction.distance(); move++) {
                switch (instruction.direction()) {
                    case "R":
                        currentX ++;
                        break;
                    case "L":
                        currentX --;
                        break;
                    case "U":
                        currentY ++;
                        break;
                    case "D":
                        currentY --;
                        break;
                }
                steps ++;
                visitedPoints.put(new Point(currentX, currentY), steps);
            }
        }

        return visitedPoints;
    }

}

