package com.alexcorrigan.adventOfCode.year2018.day2;

public class BoxID {

    private final String id;

    public BoxID(String id) {
        this.id = id;
    }

    public String id() {
        return id;
    }

    public BoxIDComparisonResult compareTo(BoxID otherBoxID) {
        int differences = 0;
        StringBuilder commonLetters = new StringBuilder();
        for (int i = 0; i < id.length(); i++) {
            char charFromThisBoxID = id.charAt(i);
            char charFromOtherBoxID = otherBoxID.id.charAt(i);
            if (charFromThisBoxID != charFromOtherBoxID) {
                differences ++;
            } else {
                commonLetters.append(charFromThisBoxID);
            }
        }
        return new BoxIDComparisonResult(this, otherBoxID, commonLetters.toString(), differences);
    }

    @Override
    public String toString() {
        return id;
    }
}
