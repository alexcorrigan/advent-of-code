package com.alexcorrigan.adventOfCode.year2019.common;

import static java.util.Arrays.stream;

public class ComputerUtils {

    public static int[] parseProgram(String programString) {
        return stream(programString.split(",")).mapToInt(Integer::valueOf).toArray();
    }

}
