import pprint
from hamcrest import *
import re

pp = pprint.PrettyPrinter()

with open('input.txt', 'r') as input_file:
    input_lines = [int(line.strip()) for line in input_file.readlines()]
    input_file.close()

test_input = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4]

def adapter_chain(adapters, difference_chain, source_rating, device_rating):
    adapters.sort()
    if len(adapters) > 0:
        for adapter_rating in adapters:
            j_diff = adapter_rating - source_rating
            if 0 < j_diff <= 3:
                difference_chain.append(j_diff)
                adapters.remove(adapter_rating)
                adapter_chain(adapters, difference_chain, adapter_rating, device_rating)
                break
    else:
        j_diff = device_rating - source_rating
        difference_chain.append(j_diff)
    return difference_chain

def count_jolt_differences(differences):
    diff_map = {}
    diff_map[1] = len([n for n in differences if n == 1])
    diff_map[2] = len([n for n in differences if n == 2])
    diff_map[3] = len([n for n in differences if n == 3])
    return diff_map

def adapter_diff(adapters):
    adapters.sort()
    device_rating = max(adapters) + 3
    chain = adapter_chain(adapters, [], 0, device_rating)
    diff_counts = count_jolt_differences(chain)
    return diff_counts[1] * diff_counts[3]
    
part_1_test_result = adapter_diff(list(test_input))
assert_that(part_1_test_result, is_(35))
print(f"Part 1 Test = {part_1_test_result}")

part_1_result = adapter_diff(list(input_lines))
assert_that(part_1_result, is_(2368))
print(f"Part 1 = {part_1_result}")

def valid_pairing(a1, a2):
    return 0 < a2 - a1 <= 3


def combinations(adapters):
    adapters.append(0)
    adapters.append(max(adapters) + 3)
    adapters.sort()

    combos = [0] * (max(adapters) + 1)
    combos[0] = 1

    for i in range(1, max(adapters) + 1):
        for j in range(1, 4):
            if (i - j) in adapters:
                combos[i] += combos[i - j]

    return combos[-1]

part_2_test_result = combinations(list(test_input))
assert_that(part_2_test_result, is_(8))
print(f"Part 2 Test = {part_2_test_result}")

part_2_result = combinations(list(input_lines))
assert_that(part_2_result, is_(1727094849536))
print(f"Part 2 = {part_2_result}")
