package com.alexcorrigan.adventOfCode.year2018.day1.deviceCalibrationStrategy;

import com.alexcorrigan.adventOfCode.year2018.day1.Frequency;
import com.alexcorrigan.adventOfCode.year2018.day1.FrequencyChange;
import com.alexcorrigan.adventOfCode.year2018.day1.FrequencyChangeSet;

import java.util.ArrayList;
import java.util.List;

public class DuplicateFrequencyDeviceCalibrationStrategy implements DeviceCalibrationStrategy {

    private final List<Frequency> encounteredFrequencies = new ArrayList<>();

    @Override
    public Frequency calibrate(Frequency initialFrequency, FrequencyChangeSet frequencyChangeSet) {
        boolean foundDuplicateFrequency = false;
        Frequency currentFrequency = initialFrequency;
        encounteredFrequencies.add(currentFrequency);
        while (!foundDuplicateFrequency) {
            for (FrequencyChange frequencyChange : frequencyChangeSet) {
                currentFrequency = currentFrequency.change(frequencyChange);
                if (frequencyEncounteredTwice(currentFrequency)) {
                    foundDuplicateFrequency = true;
                    break;
                } else {
                    encounteredFrequencies.add(currentFrequency);
                }
            }
        }
        return currentFrequency;
    }

    private boolean frequencyEncounteredTwice(Frequency frequency) {
        return encounteredFrequencies.contains(frequency);
    }

}
