package com.alexcorrigan.adventOfCode.year2019.day3;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static com.alexcorrigan.adventOfCode.common.DayOfAdvent.DAY_3;
import static com.alexcorrigan.adventOfCode.common.InputLoader.loadInputData;
import static org.junit.Assert.assertEquals;

public class Day3Test {

    private WiringReport wiringReport;

    @Before
    public void setUp() throws Exception {
        List<String> wirePaths = loadInputData(DAY_3, "2019");
        wiringReport = new FuelManagementSystem(wirePaths.get(0), wirePaths.get(1)).wiringReport();
    }

    @Test
    public void testPart1() {
        assertEquals(232, wiringReport.intersectionClosestToCentralPort().manhattanDistanceFromCentralPort());
    }

    @Test
    public void testPart2() {
        assertEquals(6084, wiringReport.intersectionLeastStepsToCentralPort().combinedStepsFromCentralPort());
    }

}
