package com.alexcorrigan.adventOfCode.year2019.day1;

public class FuelCalculator {

    private final FuelCalculatorStrategy strategy;

    public FuelCalculator(FuelCalculatorStrategy strategy) {
        this.strategy = strategy;
    }

    public Integer fuelForModuleMass(Integer moduleMass) {
        return strategy.fuelForModuleOfMass(moduleMass);
    }

}
