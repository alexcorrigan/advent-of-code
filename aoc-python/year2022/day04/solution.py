import re
from os import path

from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(path.dirname(__file__))
        assignment_pairs = self.raw_input.splitlines()
        self.assignment_ranges = []
        for assignment_pair in assignment_pairs:
            self.assignment_ranges.append(self.build_assignment_ranges(assignment_pair))

    @staticmethod
    def build_assignment_ranges(assignment_pair):
        parts = re.search(r"(\d*)-(\d*),(\d*)-(\d*)", assignment_pair)

        assignment_a_start = int(parts.group(1))
        assignment_a_end = int(parts.group(2))
        assignment_b_start = int(parts.group(3))
        assignment_b_end = int(parts.group(4))

        assignment_a_range = []
        assignment_b_range = []

        for a in range(assignment_a_start, assignment_a_end + 1):
            assignment_a_range.append(a)

        for b in range(assignment_b_start, assignment_b_end + 1):
            assignment_b_range.append(b)

        return assignment_a_range, assignment_b_range

    def part_1(self):
        overlapping_assignments = 0
        for assignment_a_range, assignment_b_range in self.assignment_ranges:
            if all([a in assignment_b_range for a in assignment_a_range]) or all([b in assignment_a_range for b in assignment_b_range]):
                overlapping_assignments += 1

        return overlapping_assignments

    def part_2(self):
        overlapping_assignments = 0
        for assignment_a_range, assignment_b_range in self.assignment_ranges:
            if any([a in assignment_b_range for a in assignment_a_range]) or any([b in assignment_a_range for b in assignment_b_range]):
                overlapping_assignments += 1

        return overlapping_assignments


if __name__ == '__main__':
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
