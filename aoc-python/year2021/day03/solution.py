import os
from base_solution import BaseSolution


class Solution(BaseSolution):

    def __init__(self):
        super().__init__(os.path.dirname(__file__))

    def part_1(self, raw_input=None):
        report = self.raw_input.splitlines() if not raw_input else raw_input.splitlines()

        gama_rate = ''
        epsilon_rate = ''

        for position in range(0, len(report[0])):
            bits = [reading[position] for reading in report]
            most_common_bit = self._bit_meeting_criteria(bits, 'most_common')
            gama_rate = gama_rate + most_common_bit

        for bit in gama_rate:
            epsilon_rate = epsilon_rate + '0' if bit == '1' else epsilon_rate + '1'

        return int(gama_rate, 2) * int(epsilon_rate, 2)

    def part_2(self, raw_input=None):
        base_report = self.raw_input.splitlines() if not raw_input else raw_input.splitlines()

        oxygen_generator_rating = self._find_rating_by_bit_criteria(base_report, 'most_common')
        co2_scrubber_rating = self._find_rating_by_bit_criteria(base_report, 'least_common')

        return oxygen_generator_rating * co2_scrubber_rating

    @staticmethod
    def _find_rating_by_bit_criteria(report, bit_criteria):
        position = 0
        while len(report) > 1 and position < len(report[0]):
            bits = [reading[position] for reading in report]
            bit_to_filter = Solution._bit_meeting_criteria(bits, bit_criteria)
            report = Solution._filter_report_at_position(report, position, bit_to_filter)
            position += 1
        return int(report[0], 2)

    @staticmethod
    def _filter_report_at_position(report, position, bit):
        new_report = []
        for reading in report:
            if reading[position] == bit:
                new_report.append(reading)
        return new_report

    @staticmethod
    def _bit_meeting_criteria(bits, bit_criteria):
        bit_counts = Solution._count_bits(bits)
        if bit_criteria == 'most_common':
            return '0' if bit_counts['0'] > bit_counts['1'] else '1'
        elif bit_criteria == 'least_common':
            return '1' if bit_counts['1'] < bit_counts['0'] else '0'
        else:
            raise Exception(f'unknown bit_criteria: {bit_criteria}')

    @staticmethod
    def _count_bits(bits):
        counts = {'0': 0, '1': 0}
        for bit in bits:
            if bit == '0':
                counts['0'] += 1
            else:
                counts['1'] += 1
        return counts


if __name__ == '__main__':
    readings = "00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010"
    solution = Solution()
    print(f"Part 1 = {solution.part_1()}")
    print(f"Part 2 = {solution.part_2()}")
