package com.alexcorrigan.adventOfCode.year2018.day1;

import java.util.Objects;

public class SimpleFrequency implements Frequency {

    private final Integer value;

    public SimpleFrequency(Integer value) {
        this.value = value;
    }

    @Override
    public Frequency change(FrequencyChange frequencyChange) {
        return new SimpleFrequency(frequencyChange.applyTo(value));
    }

    @Override
    public Integer toInteger() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) && (obj instanceof SimpleFrequency) && (((SimpleFrequency) obj).value.equals(value));
    }
}
