package com.alexcorrigan.adventOfCode.year2017.day4;

public class PassphraseValidityChecker {

    public Boolean isValid(Passphrase passphrase, ValidityStrategy validityStrategy) {
        return validityStrategy.isValid(passphrase);
    }
}
