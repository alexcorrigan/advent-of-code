
puzzle_input = range(278384, 824795)


def is_valid(test, part):
    previous = None
    digit_counts = {}
    for i in str(test):
        i = int(i)
        if previous is None or i >= previous:
            if i in digit_counts.keys():
                digit_counts[i] = digit_counts[i] + 1
            else:
                digit_counts[i] = 1
            previous = i
        else:
            return False
    at_least_one_repeating_digit = [digit_count for digit_count in digit_counts.values() if digit_count > 1]
    at_least_one_pair_of_digits = [digit_count for digit_count in digit_counts.values() if digit_count == 2]
    if part == 1:
        return at_least_one_repeating_digit
    elif part == 2:
        return at_least_one_pair_of_digits


part_1_valid_passwords = [candidate for candidate in puzzle_input if is_valid(candidate, 1)]
assert len(part_1_valid_passwords) == 921

part_2_valid_passwords = [candidate for candidate in puzzle_input if is_valid(candidate, 2)]
assert len(part_2_valid_passwords) == 603

