package com.alexcorrigan.adventOfCode.year2017.day4;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NoDuplicatedWordsValidityStrategy implements ValidityStrategy {

    @Override
    public boolean isValid(Passphrase passphrase) {
        List<String> passphraseElements = passphrase.getPassphraseElements();
        Set<String> deduplicatedPassphraseElements = new HashSet<>(passphraseElements);
        return passphraseElements.size() == deduplicatedPassphraseElements.size();
    }

}
