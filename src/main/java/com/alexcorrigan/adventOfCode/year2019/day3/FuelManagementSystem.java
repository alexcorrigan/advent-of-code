package com.alexcorrigan.adventOfCode.year2019.day3;

import com.google.common.collect.Sets;

import java.util.*;

public class FuelManagementSystem {

    private final Path blueWirePath;
    private final Path redWirePath;

    public FuelManagementSystem(String blueWirePathString, String redWirePathString) {
        this(new Path(blueWirePathString), new Path(redWirePathString));
    }

    public FuelManagementSystem(Path blueWirePath, Path redWirePath) {
        this.blueWirePath = blueWirePath;
        this.redWirePath = redWirePath;
    }

    public WiringReport wiringReport() {
        Map<Point, Integer> blueWirePoints = blueWirePath.traversePath();
        Map<Point, Integer> redWirePoints = redWirePath.traversePath();

        Set<Point> intersections = Sets.intersection(blueWirePoints.keySet(), redWirePoints.keySet());

        List<IntersectionReport> intersectionReports = new ArrayList<>();

        intersections.forEach(i -> intersectionReports.add(new IntersectionReport(i, i.manhattanDistanceFromOrigin(), blueWirePoints.get(i), redWirePoints.get(i))));

        return new WiringReport(intersectionReports);
    }
}
