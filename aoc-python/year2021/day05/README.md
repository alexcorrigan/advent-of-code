# Day 5

Finally, did some visualisations:

## Part 1
![Part 1 vent layouts](part01.png "Part 1 vent layouts")

## Part 2
![Part 2 vent layouts](part02.png "Part 2 vent layouts")