from intcode_computer.computer import Computer


def test_program(program, noun=None, verb=None):
    computer = Computer(program)
    return computer.execute_intcode_program(noun=noun, verb=verb)


assert str(test_program("1,0,0,0,99")) == "[2, 0, 0, 0, 99]"
assert str(test_program("2,3,0,3,99")) == "[2, 3, 0, 6, 99]"
assert str(test_program("2,4,4,5,99,0")) == "[2, 4, 4, 5, 99, 9801]"
assert str(test_program("1,1,1,4,99,5,6,0,99")) == "[30, 1, 1, 4, 2, 5, 6, 0, 99]"

day_2_program = open("input.txt", 'r').readlines()[0]

# Part 1
output = test_program(day_2_program, noun=12, verb=2)
assert output[0] == 3765464
print("Part 1 = %s" % output[0])

# Part 2
result = None
for n in range(99):
    for v in range(99):
        output = test_program(day_2_program, noun=n, verb=v)
        if output[0] == 19690720:
            result = 100 * n + v
            break

assert result == 7610
print("Part 2 = %s" % str(result))

